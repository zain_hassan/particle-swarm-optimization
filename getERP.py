# -*- coding: utf-8 -*-
"""
Honors' Project
    "Combinatorial Multimodal Optimization using Artificial Life Algorithms"

    Supervisor: Gesche Fensder, Professorship of Computational Mechanics, Technical University of Munich
    Author:     Zain Hassan
    Date:       3 March 2017
    
Species-based Fitness Sharing Combinatorial Particle Swarm Optimization for multimodal optimization(FS-SpeicesCPSO):
    -Combinatorial PSO adapted from 'Combinatorial PSO for partitional clustering problem, Jarboui et al (2007), Elsevier'
    -Species PSO adapted from 'Adaptively Choosing Neighbourhood Bests Using Species in a Particle Swarm Optimizer for Multimodal Function Optimization, Xiaodong Li (2004), Springer'
    -Fitness sharing adapted from 'PSO with sharing for Multimodal function optimization, Li et al (2003)'
    -Inertia reduction idea taken from 'A Dynamic Inertia weight PSO algorithm, Jiao et al (2006), Elsevier'
    -repair, getERP and plotmydesign function adapted from Differential Evolution algorithm written by Gesche Fender  as a part of PhD thesis at Technical University of Munich    

getERP
"""

from abaqus import *
from abaqusConstants import *
from odbAccess import *
from textRepr import *
import numpy as np
#import sys

Z_Luft=413.5 #rho times c at 20 degree Celsius
numbersteps=2
elementsize=(0.5*0.5)      #elementsize in squaremeters
normalDirection=2    # [0]=u1, [1]=u2, [2]=u3
    
print ('Starting ERP Calculation')

myodb=openOdb(path='d:/FGCM/Zain_Hassan/Optimization/input_temp.odb')
step1=myodb.steps.values()[0]

ERP=[]
frequency=[]

for framenumber in range(numbersteps):
    displacement= myodb.steps['Harmonic Analysis'].frames[framenumber].fieldOutputs['U']
    frequency_temp=myodb.steps['Harmonic Analysis'].frames[framenumber].frequency
    
    #set lowerpanel is not all nodes of lower panel!
    panel = myodb.rootAssembly.nodeSets['NODESPANEL']
    
    uNormal=[]
    
    panelDisplacement = displacement.getSubset(region=panel)
    panelValues = panelDisplacement.values
    for v in panelValues:
        uNormal.append(np.square(v.data[normalDirection]))   # [0]=u1, [1]=u2, [2]=u3
          
    uNormalsum_temp=np.sum(uNormal)
    #weight is 4*0.25*elementsize=elementsize for linear
    #weight for quadratic: field output set to "external" in abaqus, thus same as linear
    ERP_temp=np.square(2*np.pi*frequency_temp)*Z_Luft*uNormalsum_temp*elementsize
    #print u2sum
    ERP.append(ERP_temp)
    frequency.append(frequency_temp)

myodb.close()

data=[frequency, ERP]
print (data)

f = open("d:/FGCM/Zain_Hassan/Optimization/ERP_temp.txt", "w")
f.write(str(data))
f.close()

print ('done computing ERP ' )
