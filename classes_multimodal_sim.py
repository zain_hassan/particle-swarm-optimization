# -*- coding: utf-8 -*-
"""
Honors' Project
    "Combinatorial Multimodal Optimization using Artificial Life Algorithms"

    Supervisor: Gesche Fensder, Professorship of Computational Mechanics, Technical University of Munich
    Author:     Zain Hassan
    Date:       3 March 2017
    
Species-based Fitness Sharing Combinatorial Particle Swarm Optimization for multimodal optimization(FS-SpeicesCPSO):
    -Combinatorial PSO adapted from 'Combinatorial PSO for partitional clustering problem, Jarboui et al (2007), Elsevier'
    -Species PSO adapted from 'Adaptively Choosing Neighbourhood Bests Using Species in a Particle Swarm Optimizer for Multimodal Function Optimization, Xiaodong Li (2004), Springer'
    -Fitness sharing adapted from 'PSO with sharing for Multimodal function optimization, Li et al (2003)'
    -Inertia reduction idea taken from 'A Dynamic Inertia weight PSO algorithm, Jiao et al (2006), Elsevier'
    -repair, getERP and plotmydesign function adapted from Differential Evolution algorithm written by Gesche Fender  as a part of PhD thesis at Technical University of Munich    
    
Classes
"""
import numpy as np
import random, math, os
import matplotlib.pyplot as plt

class particle:
    
    @classmethod
    def initialize_global_variables(cls, length, upper_bound,lower_bound, maximize, inertia, c_p, c_g, alpha):
        particle.length = length
        particle.upper_bound = upper_bound        
        particle.lower_bound = lower_bound
        particle.gbest = np.zeros(length)
        particle.gbest_fitness = float('NaN')
        particle.maximize = maximize
        particle.number_of_simulations = int(0)
        particle.set_inertia(inertia)
        particle.set_c_p(c_p ) 
        particle.set_c_g(c_g) 
        particle.set_alpha(alpha)
    
    @classmethod 
    def set_inertia(cls, inertia=0.9):
        particle.inertia = inertia
        
    @classmethod    
    def set_c_p(cls, c_p=0.5):
        particle.c_p = c_p
        
    @classmethod    
    def set_c_g(cls, c_g=0.5):
        particle.c_g = c_g
        
    @classmethod    
    def set_alpha(cls, alpha=0.5):
        particle.alpha = alpha
        
    @classmethod
    def update_inertia(cls, g, max_gen, limit):
        if (g>limit):                                                                #dynamic reduction of inertia    
            particle.set_inertia(particle.inertia*np.exp(-4*(g-limit)/(max_gen-limit)))           #particle.set_inertia(inertia*(1-1/max_gen*g)) #particle.set_inertia(inertia*np.pow(1.005,-g))

    def __init__(self,ID):    
        self.ID = int(ID)
        self.subswarm_ID = -1
        self.isseed = False
        
        self.position = np.empty(particle.length,dtype=int)
        for i in range (particle.length):
            self.position[i] =  random.randrange(particle.lower_bound,particle.upper_bound,1)
        self.repair()
        
        self.original_fitness = float('NaN')
        self.shared_fitness = float('NaN')
        
        self.pbest_shared = np.copy(self.position)
        self.pbest_fitness_shared = float('NaN')
        
        self.pbest_original = np.copy(self.position)
        self.pbest_fitness_original = float('NaN')
        
        self.lbest_shared = np.copy(self.position)
        self.lbest_fitness_shared = float('NaN')
        
        self.lbest_original = np.copy(self.position)
        self.lbest_fitness_original = float('NaN')
        
        self.transformed_position = self.transform()
        
        self.velocity = np.zeros(particle.length)
        for i in range (particle.length):                                      #initialization with a random number 
            self.velocity[i] = 1-2*random.random()                             #between -1 and 1         
    
    #main SOLVE function            
    def move_and_update(self,log):
        self.transform()                                                       #transform the position into space of (-1,0,1)
        self.update_velocity()                                                 #update velocity based on pbest and gbest
        self.update_transformed_position()                                     #get new transformed position from new velocity
        self.inverse_transform()                                               #transform the position back to integer space
        self.repair()                                                          #check for repititions in position and replace them with randoml numbers
        self.evaluate_fitness(log)                                             #evaluate fitness of the new position 
        
    #transforming the design vector        
    def transform (self):
        transformed_position = np.zeros(particle.length)        
        for i in range (particle.length):
            if self.position[i] == self.lbest_shared[i] and self.position[i] == self.pbest_shared[i]:             
                transformed_position[i]= random.randrange(-1,2,1)
            elif self.position[i] == self.lbest_shared[i]:
                transformed_position[i]=int(1)
            elif self.position[i] == self.pbest_shared[i]:
                transformed_position[i]=int(-1)
            else:
                transformed_position[i] = 0
        self.transformed_position = transformed_position
        
    #transforming back to design vector        
    def inverse_transform (self):
        
        position = np.zeros(particle.length)
        
        for i in range (particle.length):
            if self.transformed_position[i] == int(1):
                position[i] = self.lbest_shared[i]
            elif self.transformed_position[i] == int(-1):
                position[i] = self.pbest_shared[i]
            else:
                position[i] = random.randint(particle.lower_bound,particle.upper_bound)
        self.position = position
        
    #repair function adapted from Gesche's Differential Evolution code
    def repair(self):
        bucket = np.arange(particle.lower_bound,(particle.upper_bound)+1,1)             #bucket is a set of all feasible solution components
        self.position = np.unique(self.position)                                        #remove duplications
        for k in self.position:
            if k < particle.lower_bound or k > particle.upper_bound:
                self.position = np.delete(self.position,np.argwhere(self.position==k))  #remove out-of-bound entries
        for i in self.position:
            bucket = np.delete(bucket,np.argwhere(bucket==i))                           #remove elements from bucket that are already in position vector to avoid repitition
        for j in range (particle.length - np.size(self.position)):                      #loop till position vector is same length as standard
            r = np.random.choice(bucket)
            self.position = np.append(self.position,r)                                  #pick a random from bucket and append to position
            bucket = np.delete(bucket,np.argwhere(bucket==r))                           #remove the already picked element from bucket to avoid repitition
            
        self.position = np.sort(self.position)            
        
    def update_velocity(self):
        r_p = random.random()
        r_g = random.random()
        
        for i in range (particle.length):
            self.velocity[i] = self.inertia*self.velocity[i]+ r_p*self.c_p*(-1-self.transformed_position[i])+ r_g*self.c_g*(1-self.transformed_position[i])
        
    def update_transformed_position(self):
        for i in range (particle.length):
            temp = self.transformed_position[i]+self.velocity[i]        
            if temp > self.alpha:
                self.transformed_position[i] = int(1)
            elif temp < -1*(self.alpha):
                self.transformed_position[i] = int(-1)
            else:
                self.transformed_position[i] = int(0)
       
    def update_pbest(self):
        if isbetter(self.shared_fitness,self.pbest_fitness_shared,particle.maximize) or (math.isnan(self.pbest_fitness_shared)):
            self.pbest_fitness_shared = np.copy(self.shared_fitness)
            self.pbest_shared = np.copy(self.position)
            
        if isbetter(self.original_fitness,self.pbest_fitness_original,particle.maximize) or (math.isnan(self.pbest_fitness_original)):
            self.pbest_fitness_original = np.copy(self.original_fitness)
            self.pbest_original = np.copy(self.position)
            
    def evaluate_fitness(self,log):
        for it_1 in range (len(log)):
            for it_2 in range (len(log[0].swarm[:,0])):
                if(np.sum(np.abs(np.sort(log[it_1].swarm[it_2,:])-np.sort(self.position)))==0):
                    self.original_fitness = log[it_1].fitness[it_2]
                    print ('Position already in memory, skipping simulation!')
                    return self.original_fitness
                        
        patch = []
        for i in range(particle.upper_bound-particle.lower_bound+1):
            patch.append('Material-Soft')
        for d in self.position:
            patch[int(d-1)] = 'Material-Stiff'
        
        inputfile = "input_temp"
        file_1 = open(inputfile+".inp", "w")
        with open("Problem_Input.inp") as file_2:
            lines = file_2.readlines()
            for i in range(27870):
                file_1.writelines(lines[i])
            
            for j in range (25):
                file_1.write('\n*Solid Section, elset=Patch-'+str(j)+', material='+str(patch[j])+'\n')
                file_1.write(',\n')
                
            for i in range(27945,28633):
                file_1.writelines(lines[i])
        file_1.close()
    
        #run simulation on input file
        n_cpus = 6
        s_command = 'abaqus j=' + inputfile+' cpus=' + str(n_cpus) + ' ask_delete=OFF -interactive'
        #print(s_command)
        os.system(s_command)
        
        particle.number_of_simulations += 1
        
        #read ERP from output of simulation
        os.system('abaqus cae -noGUI getERP.py')
        
        #return ERP
        file_3 = open("ERP_temp.txt","r")
        ERP = ((file_3.readline()).split())[-1]
        ERP = float(ERP[:(len(ERP)-2)])
        file_3.close()
        self.original_fitness = ERP

        #self.original_fitness = np.sum(self.position)        

    def evaluate_fitness_initial(self):
        patch = []
        for i in range(particle.upper_bound-particle.lower_bound+1):
            patch.append('Material-Soft')
        for d in self.position:
            patch[int(d-1)] = 'Material-Stiff'
        
        inputfile = "input_temp"
        file_1 = open(inputfile+".inp", "w")
        with open("Problem_Input.inp") as file_2:
            lines = file_2.readlines()
            for i in range(27870):
                file_1.writelines(lines[i])
            
            for j in range (25):
                file_1.write('\n*Solid Section, elset=Patch-'+str(j)+', material='+str(patch[j])+'\n')
                file_1.write(',\n')
                
            for i in range(27945,28633):
                file_1.writelines(lines[i])
        file_1.close()
    
        #run simulation on input file
        n_cpus = 4
        s_command = 'abaqus j=' + inputfile+' cpus=' + str(n_cpus) + ' ask_delete=OFF -interactive'
        #print(s_command)
        os.system(s_command)
        
        particle.number_of_simulations += 1        
        
        #read ERP from output of simulation
        os.system('abaqus cae -noGUI getERP.py')
        
        #return ERP
        file_3 = open("ERP_temp.txt","r")
        ERP = ((file_3.readline()).split())[-1]
        ERP = float(ERP[:(len(ERP)-2)])
        file_3.close()
        self.original_fitness = ERP
        
        #self.original_fitness = np.sum(self.position)
        
    def print_particle(self,fo,g):
        print_and_save(fo,'\ngen# '+str(g)+
                        ' subswarm# '+str(self.subswarm_ID)+
                        ' particle# '+str(self.ID)+
                        ' position: '+str(self.position)+
                        ' fitness: '+str(self.original_fitness)+
                        ' Shared fitness: '+str(self.shared_fitness));
        
    def print_lbest(self,fo):
        print_and_save(fo,
                   '\n---------------------------------------\nsubswarm# '+
                   str(self.subswarm_ID)+
                   ' lbest: '+str(self.lbest_original)+
                   ' lbest fitness: '+str(self.lbest_fitness_original)+
                   '\n')
                       
    def print_plot_lbest(self,fo,rows,columns):    
        print_and_save(fo,'\nsubswarm# '+str(self.subswarm_ID)+
                ' lbest: '+str(self.lbest_original)+
                ' lbest_fitness: '+str(self.lbest_fitness_original))
        plotmydesign(self.lbest_shared,rows,columns,
                'lbest of Subswarm# '+str(self.subswarm_ID)+
                ' fitness = '+str(self.lbest_fitness_original))	
        
    @classmethod
    def print_number_of_function_calls(cls,fo):
        print_and_save(fo,
           '\n###############################################\nNumber of Simulation calls for objective function evaluations '+
           str(particle.number_of_simulations)+'\n###############################################')
  
#can not import these methods from functions.py, hence defining them again          
#----------------------------------------------
def isbetter(a,b,maximize):
    if (maximize):        
        if(math.isnan(a)):
            return False
        if(math.isnan(b)):
            return True
        if(a>b):
            return True
        if(a<b):
            return False

    elif (not maximize):
        if(math.isnan(a)):
            return True
        if(math.isnan(b)):
            return False
        if(a>b):
            return False
        if(a<b):
            return True
    
    else:
        return False
        
def print_and_save(fo,string):
    fo.write(string);
    print(string)
    
def plotmydesign(design,rows,columns,title='No Title'):
    xlist = []
    ylist = []    
    for m in range (columns+1):
        xlist.append(m)        
    for n in range (rows+1):
        ylist.append(n)
        
    plt.figure()
    plt.xlim(0,rows)
    plt.ylim(0,columns)
    plt.xticks(xlist)
    plt.yticks(ylist)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.grid(True, color='w', linestyle='-', linewidth=2)
    plt.gca().patch.set_facecolor('0.8')
    plt.gca().axes.xaxis.set_ticklabels([])
    plt.gca().axes.yaxis.set_ticklabels([])
    plt.text(0,-0.5,title)
    for d in design:
        i = int(((d-1)%columns)+1)
        j = int(((d-i-1)/columns)+2)
        plt.text(i-0.5, j-0.5, "io", size=30, rotation=0.,ha="center", 
                 va="center",bbox=dict(boxstyle="square",facecolor='k'))                             
    plt.savefig('run\\'+title+'.png',dpi=100)
    plt.show()