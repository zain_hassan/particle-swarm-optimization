# -*- coding: utf-8 -*-
"""
Honors' Project
    "Combinatorial Multimodal Optimization using Artificial Life Algorithms"

    Supervisor: Gesche Fensder, Professorship of Computational Mechanics, Technical University of Munich
    Author:     Zain Hassan
    Date:       3 March 2017
    
Species-based Fitness Sharing Combinatorial Particle Swarm Optimization for multimodal optimization(FS-SpeicesCPSO):
    -Combinatorial PSO adapted from 'Combinatorial PSO for partitional clustering problem, Jarboui et al (2007), Elsevier'
    -Species PSO adapted from 'Adaptively Choosing Neighbourhood Bests Using Species in a Particle Swarm Optimizer for Multimodal Function Optimization, Xiaodong Li (2004), Springer'
    -Fitness sharing adapted from 'PSO with sharing for Multimodal function optimization, Li et al (2003)'
    -Inertia reduction idea taken from 'A Dynamic Inertia weight PSO algorithm, Jiao et al (2006), Elsevier'
    -repair, getERP and plotmydesign function adapted from Differential Evolution algorithm written by Gesche Fender  as a part of PhD thesis at Technical University of Munich    
    -Test functions taken from 'https://www.sfu.ca/~ssurjano/optimization.html' (accessed on: 06 April 2017)
    
Main File
"""
import numpy as np
from classes_multimodal_test import particle
from archive import archive
from functions import write_header_to_file_multimodal, divide_into_subswarms_shared, assemble_main_swarm, print_gen, plot_log_test, share_fitness_subswarm, plot_points_final_test, plot_lbests_test, plot_points_test

#************INITIALIZATION************
maximize = False

upper_bound = 2500                   
lower_bound = -2500
range_ = upper_bound-lower_bound
dimension = 2

#obj_fxn = 'Ackley'
obj_fxn = 'Rastrigin_lo'
#obj_fxn = 'Rastrigin_hi'
#obj_fxn = 'Griewank'
#obj_fxn = 'Schwefel'
#obj_fxn = 'Sphere'
#obj_fxn = 'Styblinski-Tang'
#'''

#set parameters of SCPSO
swarm_size = 75                                                                #swarm size = number of particles (default: equal to or greater than range) 
max_gen = 250                                                                  #maximum number of generations     
             
subswarm_radius =range_/10									     #radius of a subswarm around a seed in Euclidean distance, setting to zero will make the algorithm work with only 1 swarm
subswarm_frequency = 99999                                                     #make subswarms after how many generations
min_no_subswarms = 7
sharing_radius = 250        

inertia = 0.85         #0.85                                                   #inertia for velocity
c_p = 0.6              #0.6                                                    #coefficient for cognitive behaviour
c_g = 0.4              #0.4                                       	          #coefficient for social behaviour
alpha = 7/swarm_size   #0.35                                                   #factor for inverse transformation in CPSO

archive.initialize_global_variables(dimension,swarm_size,maximize)
particle.initialize_global_variables(dimension,upper_bound,lower_bound,maximize,
                                     inertia, c_p, c_g, alpha, obj_fxn)

#file for writing results
fo = write_header_to_file_multimodal(swarm_size,max_gen,subswarm_radius,
                                subswarm_frequency,inertia,c_p,c_g,alpha,
                                2,1,min_no_subswarms)

#set up achives
log = np.empty((max_gen),dtype=object)
for it_1 in range (max_gen):
    log[it_1] = archive()  
    
#initialize first main swarm and global variables
main_swarm = np.empty((swarm_size), dtype=object)
for it_0 in range (swarm_size):
    main_swarm[it_0]= particle(it_0)
    
#divide the main swarm into subswarms
subswarm = divide_into_subswarms_shared(swarm_size,subswarm_radius, 
                                        min_no_subswarms, main_swarm,
                                        maximize, 5)                   

#evaluate initial fitnesses of all particles
for i in range(len(subswarm)):
    for j in range(np.size(subswarm[i])):
        subswarm[i][j].evaluate_fitness()
    
#set the shared fitnesses, pbests and lbests of all particles
share_fitness_subswarm(subswarm,maximize,sharing_radius, 5) 

#************SOLUTION************
for g in range(max_gen):                                                       #Loop over generations
    print_gen(fo, g)
    particle.update_inertia(g, max_gen, max_gen/2)
    
    #plot_points_test(subswarm,g)

    for i in range(len(subswarm)):                                             #Loop over subswarms
        subswarm[i][0].print_lbest(fo)        
        for j in range(np.size(subswarm[i])):
            
            log[g].store(subswarm[i][j])                                       #archiving
            subswarm[i][j].print_particle(fo,g+1)            
            archive.update_gbest(subswarm[i][j]) 
    
            #SOLVE
            if ((g+1)!=max_gen):
                subswarm[i][j].move_and_update()                                   #solve function based on CPSO
    #Splitting of main swarm into subswarms           
    if ((g+1)%subswarm_frequency ==0 and (g+1)!=max_gen):
        main_swarm = assemble_main_swarm(swarm_size,subswarm)                  #assemble subswarms into main swarm, needed for re-splitting
        subswarm = divide_into_subswarms_shared(swarm_size,subswarm_radius, 
                                               min_no_subswarms, main_swarm,
                                               maximize)                       #based on Species based PSO
    
    #set the shared fitnesses, pbests and lbests of all particles
    share_fitness_subswarm(subswarm,maximize,sharing_radius,5)

#************POST-PROCESSING************
    log[g].store_gbest_fitness(archive.gbest_fitness)                          #save the global best at this generation
    archive.print_gbest(fo)

#Plot lbests
v1 = []
v2 = []
for it_3 in range(len(subswarm)):    
    subswarm[it_3][0].print_lbest(fo)
    v1.append(subswarm[it_3][0].lbest_original[0])
    v2.append(subswarm[it_3][0].lbest_original[1])
plot_lbests_test(upper_bound, lower_bound, v1,v2,obj_fxn)

#plot_points_final_test(subswarm,lower_bound,upper_bound,obj_fxn)
archive.print_optimum(fo)

plot_log_test(log)

fo.close()