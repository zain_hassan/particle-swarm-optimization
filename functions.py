# -*- coding: utf-8 -*-
"""
Honors' Project
    "Combinatorial Multimodal Optimization using Artificial Life Algorithms"

    Supervisor: Gesche Fensder, Professorship of Computational Mechanics, Technical University of Munich
    Author:     Zain Hassan
    Date:       3 March 2017
    
Species-based Fitness Sharing Combinatorial Particle Swarm Optimization for multimodal optimization(FS-SpeicesCPSO):
    -Combinatorial PSO adapted from 'Combinatorial PSO for partitional clustering problem, Jarboui et al (2007), Elsevier'
    -Species PSO adapted from 'Adaptively Choosing Neighbourhood Bests Using Species in a Particle Swarm Optimizer for Multimodal Function Optimization, Xiaodong Li (2004), Springer'
    -Fitness sharing adapted from 'PSO with sharing for Multimodal function optimization, Li et al (2003)'
    -Inertia reduction idea taken from 'A Dynamic Inertia weight PSO algorithm, Jiao et al (2006), Elsevier'
    -repair, getERP and plotmydesign function adapted from Differential Evolution algorithm written by Gesche Fender  as a part of PhD thesis at Technical University of Munich    
    
Functions
"""

import time, math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import axes3d

from archive import archive

#GENERAL FUNCTIONS
def isbetter(a,b,maximize):
    if (maximize):        
        if(math.isnan(a)):
            return False
        if(math.isnan(b)):
            return True
        if(a>b):
            return True
        if(a<b):
            return False

    elif (not maximize):
        if(math.isnan(a)):
            return True
        if(math.isnan(b)):
            return False
        if(a>b):
            return False
        if(a<b):
            return True
    
    else:
        return False

def dist_patch(vector_1,vector_2,columns):
    vector1 = np.copy(vector_1)
    vector2 = np.copy(vector_2)
      
    if len(vector1)!=len(vector2):
        return 0.
    else:
        length = len(vector1)
        result = 0.
        for it_1 in range(length):
            for it_2 in range(length):
                if (vector1[it_1] == vector2[it_2]):
                    vector1[it_1] = 0
                    vector2[it_2] = 0
                    
        vector1 = np.sort(vector1)
        vector2 = np.sort(vector2)             
        
        for it_3 in range(length):
            i = vector1[it_3]
            j = vector2[it_3]
            
            i_x = int(((i-1)%columns)+1)
            i_y = int(((i-i_x)/columns)+1)
            
            j_x = int(((j-1)%columns)+1)
            j_y = int(((j-j_x)/columns)+1)

            result += (i_y - j_y)**2 + (i_x - j_x)**2
        return np.sqrt(result)    

def dist_eucl(vector1,vector2):
    if len(vector1)!=len(vector2):
        return 0.
    else:
        result = 0.
        vector1 = np.sort(vector1)
        vector2 = np.sort(vector2)        
        for i in vector1:
            for j in vector2:
                result += ((i-j)*(i-j))
        return np.sqrt(result)

#FITNESS SHARING FUNCTIONS
def share_fitness(swarm,maximize,sharing_radius,columns):
    sigma = sharing_radius

    for i in range(len(swarm)):
        m=0.001
        for j in range(len(swarm)):
            d = dist_patch(swarm[i].position,swarm[j].position,columns)            
            if (d>sigma):
                m += 0
            else:
                m += 1-(d/sigma)

        if (not maximize):    
            swarm[i].shared_fitness = swarm[i].original_fitness*m
    
        if (maximize):
            swarm[i].shared_fitness = swarm[i].original_fitness/m
        
def share_fitness_subswarm(subswarm,maximize,sharing_radius,columns):
    sigma = sharing_radius        
    for i_1 in range(len(subswarm)):
        for j_1 in range(np.size(subswarm[i_1])):
            m = 0.001
            for i_2 in range(len(subswarm)):
                if (not (i_1 == i_2)):
                    for j_2 in range(np.size(subswarm[i_2])):
                        d = dist_patch(subswarm[i_1][j_1].position,
                                       subswarm[i_2][j_2].position,columns)
                        if (d>sigma):
                            m += 0
                        else:
                            m += 1-(d/sigma)
            if (not maximize):    
                subswarm[i_1][j_1].shared_fitness = subswarm[i_1][j_1].original_fitness*m
        
            if (maximize):
                subswarm[i_1][j_1].shared_fitness = subswarm[i_1][j_1].original_fitness/m
            
            subswarm[i_1][j_1].update_pbest()    
    update_lbest(subswarm, maximize)                                           
                
#SPECIES CPSO FUNCTIONS      
def divide_into_subswarms(swarm_size,subswarm_radius,min_no_subswarms, swarm, maximize):
    radius = np.copy(subswarm_radius)    
    subswarm_ID_list = set_subswarm_ID(swarm_size,radius, swarm,maximize)    
    counter = 0
    max_counter = 200
    while(len(subswarm_ID_list)<min_no_subswarms and counter<max_counter):
        if (len(subswarm_ID_list)==1 and subswarm_ID_list[0]==0):
            radius = radius*1.15            
        else:
            radius = radius*0.99
        for i in range (len(swarm)):
            swarm[i].subswarm_ID = -1
            swarm[i].isseed = False
        subswarm_ID_list = set_subswarm_ID(swarm_size,radius, swarm, maximize)
        counter +=1
        if (counter==100):
            #min_no_subswarms -=1
            radius = radius*5
            
        if(counter==max_counter):
            print("WARNING!: Failed to make desired number of subswarms")            
            time.sleep(2)
    
    #equalize_subswarms()
    print("//////////////////////////////////////////////////")
    subswarm = [[] for n in range(len(subswarm_ID_list))]
    for it_2 in range(len(subswarm_ID_list)):
        for it_3 in range(swarm_size):
            if swarm[it_3].subswarm_ID == subswarm_ID_list[it_2]:               
                subswarm[it_2].append(swarm[it_3])
    return subswarm
    
def set_subswarm_ID (swarm_size,subswarm_radius, swarm,maximize,columns):
    
    #make a sorted list of all particel IDs and fitnesses (ascending wrt fitness)
    sorted_list = np.zeros((swarm_size,2))
    for it_1 in range(swarm_size):
        sorted_list[it_1,0] = np.copy(swarm[it_1].ID)
        sorted_list[it_1,1] = np.copy(swarm[it_1].original_fitness)            
        #sorted_list[it_1,1] = np.copy(swarm[it_1].pbest_fitness_shared)    
    sorted_list = sorted_list[sorted_list[:,1].argsort()]
    
    #Make subswarm seeds and assign subswarm groups
    if (maximize):
        for it_2 in range(swarm_size-1,-1,-1):
            found = False
            for it_3 in range (swarm_size):
                #assign to a subswarm        
                if (swarm[it_3].isseed == True and 
                dist_patch(swarm[sorted_list[it_2,0]].position,
                           swarm[it_3].position,columns)<subswarm_radius):
                    found = True
                    swarm[sorted_list[it_2,0]].subswarm_ID = swarm[it_3].subswarm_ID
                    #swarm[sorted_list[it_2,0]].set_lbest(swarm[it_3].position,swarm[it_3].fitness)
                    break
                else:    
                    pass
            #make a seed
            if found == False:
                swarm[sorted_list[it_2,0]].isseed = True
                swarm[sorted_list[it_2,0]].subswarm_ID = swarm[sorted_list[it_2,0]].ID+1
                #swarm[sorted_list[it_2,0]].set_lbest(swarm[it_3].pbest,swarm[it_3].pbest_fitness_shared)
                
    if (not maximize):
        for it_2 in range(swarm_size):
            found = False
            for it_3 in range (swarm_size):
                #assign to a subswarm        
                if (swarm[it_3].isseed == True and 
                dist_patch(swarm[sorted_list[it_2,0]].position,
                           swarm[it_3].position,columns)<subswarm_radius):
                    found = True
                    swarm[sorted_list[it_2,0]].subswarm_ID = swarm[it_3].subswarm_ID
                    #swarm[sorted_list[it_2,0]].set_lbest(swarm[it_3].position,swarm[it_3].fitness)
                    break
                else:    
                    pass
            #make a seed
            if found == False:
                swarm[sorted_list[it_2,0]].isseed = True
                swarm[sorted_list[it_2,0]].subswarm_ID = swarm[sorted_list[it_2,0]].ID+1
                #swarm[sorted_list[it_2,0]].set_lbest(swarm[it_3].pbest,swarm[it_3].pbest_fitness_shared)
        
    #Put all single seeds into one subswarm
    for it_4 in range (swarm_size):
        single = True
        for it_5 in range (swarm_size):
            if swarm[it_4].subswarm_ID == swarm[it_5].subswarm_ID and it_4!=it_5:
                single = False
            else:
                pass
        if single:
            swarm[it_4].subswarm_ID = 0
            swarm[it_4].isseed = False
   
    #make a list of all subswarm_IDs of seeds
    subswarm_IDs = np.empty(swarm_size,dtype=int)    
    for i in range (swarm_size):
        subswarm_IDs[i] = swarm[i].subswarm_ID
    subswarm_IDs = np.unique(np.sort(subswarm_IDs))
    
    print("New subswarms created with IDs: ",subswarm_IDs)
    
    return subswarm_IDs

#--------------------------
def divide_into_subswarms_shared(swarm_size,subswarm_radius,min_no_subswarms, 
                                 swarm, maximize,columns):
    radius = np.copy(subswarm_radius)    
    subswarm_ID_list = set_subswarm_ID_shared(swarm_size,radius, swarm,maximize,columns)    
    counter = 0
    max_counter = 200
    while(len(subswarm_ID_list)<min_no_subswarms and counter<max_counter):
        if (len(subswarm_ID_list)==1 and subswarm_ID_list[0]==0):
            radius = radius*1.15            
        else:
            radius = radius*0.99
        for i in range (len(swarm)):
            swarm[i].subswarm_ID = -1
            swarm[i].isseed = False
        subswarm_ID_list = set_subswarm_ID_shared(swarm_size,radius, swarm, maximize,columns)
        counter +=1
        if (counter==100):
            #min_no_subswarms -=1
            radius = radius*5
            
        if(counter==max_counter):
            print("WARNING!: Failed to make desired number of subswarms")            
            time.sleep(2)
    
    #equalize_subswarms()
    print("//////////////////////////////////////////////////")
    subswarm = [[] for n in range(len(subswarm_ID_list))]
    for it_2 in range(len(subswarm_ID_list)):
        for it_3 in range(swarm_size):
            if swarm[it_3].subswarm_ID == subswarm_ID_list[it_2]:               
                subswarm[it_2].append(swarm[it_3])
    return subswarm
    
def set_subswarm_ID_shared (swarm_size,subswarm_radius, swarm,maximize,columns):
    
    #make a sorted list of all particel IDs and fitnesses (ascending wrt fitness)
    sorted_list = np.zeros((swarm_size,2))
    for it_1 in range(swarm_size):
        sorted_list[it_1,0] = np.copy(swarm[it_1].ID)
        sorted_list[it_1,1] = np.copy(swarm[it_1].original_fitness)            
        #sorted_list[it_1,1] = np.copy(swarm[it_1].pbest_fitness_shared)    
    sorted_list = sorted_list[sorted_list[:,1].argsort()]
    
    #Make subswarm seeds and assign subswarm groups
    if (maximize):
        for it_2 in range(swarm_size-1,-1,-1):
            found = False
            for it_3 in range (swarm_size):
                #assign to a subswarm        
                if (swarm[it_3].isseed == True and 
                dist_patch(swarm[sorted_list[it_2,0]].position,
                           swarm[it_3].position,columns)<subswarm_radius):
                    found = True
                    swarm[sorted_list[it_2,0]].subswarm_ID = swarm[it_3].subswarm_ID
                    #swarm[sorted_list[it_2,0]].set_lbest(swarm[it_3].position,swarm[it_3].fitness)
                    break
                else:    
                    pass
            #make a seed
            if found == False:
                swarm[sorted_list[it_2,0]].isseed = True
                swarm[sorted_list[it_2,0]].subswarm_ID = swarm[sorted_list[it_2,0]].ID+1
                #swarm[sorted_list[it_2,0]].set_lbest(swarm[it_3].pbest,swarm[it_3].pbest_fitness_shared)
                
    if (not maximize):
        for it_2 in range(swarm_size):
            found = False
            for it_3 in range (swarm_size):
                #assign to a subswarm        
                if (swarm[it_3].isseed == True and 
                dist_patch(swarm[int(sorted_list[it_2,0])].position,
                           swarm[it_3].position,columns)<subswarm_radius):
                    found = True
                    swarm[int(sorted_list[it_2,0])].subswarm_ID = swarm[it_3].subswarm_ID
                    #swarm[sorted_list[it_2,0]].set_lbest(swarm[it_3].position,swarm[it_3].fitness)
                    break
                else:    
                    pass
            #make a seed
            if found == False:
                swarm[int(sorted_list[it_2,0])].isseed = True
                swarm[int(sorted_list[it_2,0])].subswarm_ID = swarm[int(sorted_list[it_2,0])].ID+1
                #swarm[sorted_list[it_2,0]].set_lbest(swarm[it_3].pbest,swarm[it_3].pbest_fitness_shared)
        
    #Put all single seeds into one subswarm
    for it_4 in range (swarm_size):
        single = True
        for it_5 in range (swarm_size):
            if swarm[it_4].subswarm_ID == swarm[it_5].subswarm_ID and it_4!=it_5:
                single = False
            else:
                pass
        if single:
            swarm[it_4].subswarm_ID = 0
            swarm[it_4].isseed = False
   
    #make a list of all subswarm_IDs of seeds
    subswarm_IDs = np.empty(swarm_size,dtype=int)    
    for i in range (swarm_size):
        subswarm_IDs[i] = swarm[i].subswarm_ID
    subswarm_IDs = np.unique(np.sort(subswarm_IDs))
    
    print("New subswarms created with IDs: ",subswarm_IDs)
    
    return subswarm_IDs
    
#--------------------------    
def assemble_main_swarm(swarm_size,subswarm):
    swarm = np.empty((swarm_size), dtype=object)
    for i in range(len(subswarm)):    
        for j in range(np.size(subswarm[i])):
            swarm[subswarm[i][j].ID] = subswarm[i][j]
            swarm[subswarm[i][j].ID].subswarm_ID = -1
            swarm[subswarm[i][j].ID].isseed = False
    return swarm
    
#--------------------------
def update_lbest(subswarm,maximize):
    for i in range(len(subswarm)):
        fitness_shared = np.copy(subswarm[i][0].pbest_fitness_shared)
        fitness_original = np.copy(subswarm[i][0].pbest_fitness_original)
        index_1 = 0
        for j in range(np.size(subswarm[i])-1):
            if isbetter(subswarm[i][j+1].pbest_fitness_shared,fitness_shared,maximize):
                fitness_shared = subswarm[i][j+1].pbest_fitness_shared
                fitness_original = subswarm[i][j+1].pbest_fitness_original
                index_1 = j+1
                
        for k in range(np.size(subswarm[i])):
            if (isbetter(fitness_shared,subswarm[i][k].lbest_fitness_shared,maximize) or 
            (math.isnan(subswarm[i][k].lbest_fitness_shared))):
                subswarm[i][k].lbest_fitness_shared = fitness_shared
                subswarm[i][k].lbest_shared = subswarm[i][index_1].pbest_shared
                subswarm[i][k].lbest_fitness_original = fitness_original
                subswarm[i][k].lbest_original = subswarm[i][index_1].pbest_original
def print_gen(fo, g):
    print_and_save(fo,'\n\n######################## GEN: '+str(g+1))         

#FILE WRITING FUNCTIONS  
#-------------------------
def write_header_to_file_CPSO(fo, swarm_size,max_gen,inertia,c_p,c_g,alpha,rows,columns):
    fo.write('Problem size: '+str(rows)+'x'+str(columns)+
    '\nswarm size: '+str(swarm_size)+'\nmaximum generations: '+str(max_gen)+
    '\nInertia: '+str(inertia)+
    '\nCoefficient for personal best: '+str(c_p)+
    '\nCoefficient for gbest/lbest: '+str(c_g)+
    '\nAlpha for CPSO: '+str(alpha)+
    '\n\n');

#-------------------------
def write_header_to_file_multimodal(swarm_size,max_gen,
                                    subswarm_radius,subswarm_frequency,
                                    inertia,c_p,c_g,alpha,rows,columns,
                                    min_no_subswarms):

    fo = open("run\\result.txt", "w")        
    
    fo.write('Problem size: '+str(rows)+'x'+str(columns)+
    '\nMinimum no. of subswarms: '+str(min_no_subswarms)+
    '\nswarm size: '+str(swarm_size)+'\nmaximum generations: '+str(max_gen)+
    '\nInitial Radius of subswarm: '+str(subswarm_radius)+
    '\nFrequency of subswarm creation: '+str(subswarm_frequency)+
    '\nInertia: '+str(inertia)+
    '\nCoefficient for personal best: '+str(c_p)+
    '\nCoefficient for gbest/lbest: '+str(c_g)+
    '\nAlpha for CPSO: '+str(alpha)+
    '\n\n'); 

    return fo    
    
 #-------------------------
def print_and_save(fo,string):
    fo.write(string);
    print(string)
 

#PLOTTING FUNCTIONS
def plot_log(log):
    plot_pbest_fitness(log)
    plot_gbest_fitness(log)
    plot_gbest_fitness_vs_function_calls(log)
    plot_particle_history(log,1)
   
def plot_log_test(log):
    plot_pbest_fitness(log)
    plot_gbest_fitness(log)
    plot_particle_history(log,1)
    
#--------------------------             
def plot_pbest_fitness(log):
    X = np.arange(1.,float(len(log)+1),1.)
    fig = plt.figure()
    fig.set_size_inches(12,12)
    ax=fig.add_subplot(111)
    Y = np.empty((len(log)))
    Z = np.empty((len(log)))
    for i in range (archive.swarm_size):
        for j in range ((len(log))):
            Y[j] = log[j].pbest_fitness_shared[i]
            Z[j] = log[j].pbest_fitness_original[i]
        line, = ax.plot(X,Y)
        line, = ax.plot(X,Z)
    ax.set_xlabel('generation')
    ax.set_ylabel('pbest fitness of all particles')
    title='pbest of all particles over generations'
    ax.set_title(title)
    plt.savefig('run\\'+title+'.png', dpi=100)
    plt.show()        

#-------------------------    
def plot_gbest_fitness(log):
    X = np.arange(1.,float(len(log)+1),1.)
    fig = plt.figure()   
    fig.set_size_inches(12,12)
    ax=fig.add_subplot(111)
    Y = np.empty((len(log)))
    for i in range ((len(log))):
        Y[i] = log[i].gbest_fitness
    line, = ax.plot(X,Y)
    ax.set_xlabel('generation')
    ax.set_ylabel('gbest fitness')
    title='gbest of swarm over generations'
    ax.set_title(title)
    plt.savefig('run\\'+title+'.png',dpi=100)    
    plt.show()
    
def plot_gbest_fitness_vs_function_calls(log):
    #X = np.arange(1.,float(len(log)+1),1.)
    fig = plt.figure()   
    fig.set_size_inches(12,12)
    ax=fig.add_subplot(111)
    X = np.empty((len(log)))    
    Y = np.empty((len(log)))
    for i in range ((len(log))):
        X[i] = log[i].number_of_function_calls
        Y[i] = log[i].gbest_fitness
    line, = ax.plot(X,Y)
    ax.set_xlabel('function calls')
    ax.set_ylabel('gbest fitness')
    title='gbest of swarm over number of function calls'
    ax.set_title(title)
    plt.savefig('run\\'+title+'.png',dpi=100)    
    plt.show()

#--------------------------    
def plot_particle_history(log,n=0):
    fig = plt.figure()
    fig.set_size_inches(12,12)
    ax=fig.add_subplot(111)
    
    X = np.arange(1.,float(len(log)+1),1.)
    Y = np.empty((len(log)))
    Z = np.empty((len(log)))
    
    for i in range ((len(log))):
        Y[i] = log[i].fitness[n]
        Z[i] = log[i].pbest_fitness_original[n]

    line, = ax.plot(X,Y)
    line, = ax.plot(X,Z,linewidth = 2)
    
    ax.set_xlabel('generation')
    ax.set_ylabel('fitness')
    title = 'fitness of particle '+str(n)+' over generations'
    ax.set_title(title)
    plt.savefig('run\\'+title+'.png',dpi=100)
    plt.show()
  
#--------------------------    
def plotmydesign(design,rows,columns,title='No Title'):
    xlist = []
    ylist = []    
    for m in range (columns+1):
        xlist.append(m)        
    for n in range (rows+1):
        ylist.append(n)
        
    plt.figure()
    plt.xlim(0,rows)
    plt.ylim(0,columns)
    plt.xticks(xlist)
    plt.yticks(ylist)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.grid(True, color='w', linestyle='-', linewidth=2)
    plt.gca().patch.set_facecolor('0.8')
    plt.gca().axes.xaxis.set_ticklabels([])
    plt.gca().axes.yaxis.set_ticklabels([])
    plt.text(0,-0.5,title)
    for d in design:
        i = int(((d-1)%columns)+1)
        j = int(((d-i-1)/columns)+2)
        plt.text(i-0.5, j-0.5, "io", size=30, rotation=0.,ha="center", 
                 va="center",bbox=dict(boxstyle="square",facecolor='k'))                             
    plt.savefig('run\\'+title+'.png',dpi=100)
    plt.show()

#--------------------------
def print_all_optima(log,fo,epsilon):
    n = len(log[0].swarm[0,:])    
    memory = np.empty((n,1))   
    memory[:,0] = archive.gbest
    counter=0
    for it_2 in range(len(log)):
        for it_4 in range (len(log[0].swarm[:,0])):
            if(abs(log[it_2].fitness[it_4]-archive.gbest_fitness)<=epsilon):
                new = True
                for it_3 in range (len(memory[0,:])):
                    if (np.sum(np.square(log[it_2].swarm[it_4,:]-memory[:,it_3]))==0):
                        new = False
                if new:
                    counter+=1                    
                    temp = np.empty((n,1))
                    temp[:,0] = log[it_2].swarm[it_4,:]
                    memory = np.append(memory,temp,axis=1)
                    print_and_save(fo,'\noptimum#'+str(counter)+
                                    ' position '+str(log[it_2].swarm[it_4,:])+
                                    ' fitness '+str(log[it_2].fitness[it_4]))    
    print_and_save(fo,'\nMULTIMODAL: Number of optima within +-'+str(epsilon)+
                        ' of global optimum '+str(archive.gbest_fitness)+
                        ' are '+str(counter))
    

#--------------------------
def plot_points_final_test(subswarm,lower,upper,obj):
    plt.clf()    
    
    X = np.arange(lower,upper,10)
    Y = np.arange(lower,upper,10)
    Z = np.zeros((len(X),len(Y)))
    for i in range(len(X)):
        for j in range(len(Y)):
            #Sphere function
            if (obj == 'Sphere'):        
                Z[i][j] = X[i]*X[i] + Y[j]*Y[j]
                
            #Rastrigin Function
            if (obj == 'Rastrigin_lo'):        
                Amplitude = 200
                frequency = 0.17            
                Z[i][j] = Amplitude*2+ (X[i]/100)*(X[i]/100)-Amplitude*np.cos(frequency*np.pi*(X[i]/100))+(Y[j]/100)*(Y[j]/100)-Amplitude*np.cos(frequency*np.pi*(Y[j]/100))
                
            if (obj == 'Rastrigin_hi'):        
                Amplitude = 200
                frequency = 0.35            
                Z[i][j] = Amplitude*2+ (X[i]/100)*(X[i]/100)-Amplitude*np.cos(frequency*np.pi*(X[i]/100))+(Y[j]/100)*(Y[j]/100)-Amplitude*np.cos(frequency*np.pi*(Y[j]/100))
            
            #Ackley Function
            if (obj == 'Ackley'):        
                a =20
                b = 0.2
                c = 2*np.pi
                Z[i][j] = -a * np.exp(-b*np.sqrt(1/2*(X[i]*X[i]+Y[j]*Y[j]))) - np.exp(1/2*(np.cos(c*X[i])+np.cos(c*Y[j]))) + a + np.exp(1)
            
            #Schwefel Function
            if (obj == 'Schwefel'):        
                Z[i][j] = 418.9829*2 - (X[i]/10*np.sin(np.sqrt(np.abs(X[i]/10)))) - (Y[j]/10*np.sin(np.sqrt(np.abs(Y[j]/10))))
           
            #Griewank Function
            if (obj == 'Griewank'):        
                Z[i][j] = (X[i]/1000*X[i]/1000/4000 + Y[j]*0.1*Y[j]/1000/4000) - (np.cos(X[i]/1000)*(np.cos(Y[j]/1000/(np.sqrt(2))))) +1
                
            #Styblinski-tang Function
            if (obj == 'Styblinski-Tang'):        
                Z[i][j] = 1/2 * (X[i]*X[i]*X[i]*X[i] - 16*X[i]*X[i] + 5*X[i]) + (Y[j]*Y[j]*Y[j]*Y[j] -16*Y[j]*Y[j] +5*Y[j])
                
    X, Y = np.meshgrid(X, Y)
    
    plt.figure()
    colors = iter(cm.rainbow(np.linspace(0, 1, len(subswarm))))
    for i in range(len(subswarm)):    
        v1=[]
        v2=[]
        for j in range(np.size(subswarm[i])):
            v1.append(subswarm[i][j].position[0])      
            v2.append(subswarm[i][j].position[1])      
        plt.scatter(v1,v2,color=next(colors))
    CS = plt.contour(X, Y, Z)
    plt.clabel(CS, inline=1, fontsize=10)
    plt.show
    plt.savefig('run\\final_swarm.png',dpi=100)

#--------------------------
def plot_points_test(subswarm,g):
    plt.clf()    
    colors = iter(cm.rainbow(np.linspace(0, 1, len(subswarm))))
    for i in range(len(subswarm)):    
        X=[]
        Y=[]
        for j in range(np.size(subswarm[i])):
            X.append(subswarm[i][j].position[0])      
            Y.append(subswarm[i][j].position[1])      
        plt.scatter(X,Y,color=next(colors))
    plt.savefig('run\\swarm'+str(g)+'.png',dpi=100)
    

#--------------------------
def plot_lbests_test(upper, lower,v1,v2,obj):
    X = np.arange(lower,upper,10)
    Y = np.arange(lower,upper,10)
    Z = np.zeros((len(X),len(Y)))
    for i in range(len(X)):
        for j in range(len(Y)):
            #Sphere function
            if (obj == 'Sphere'):        
                Z[i][j] = X[i]*X[i] + Y[j]*Y[j]
                
            #Rastrigin Function
            if (obj == 'Rastrigin_lo'):        
                Amplitude = 200
                frequency = 0.17       
                Z[i][j] = Amplitude*2+ (X[i]/100)*(X[i]/100)-Amplitude*np.cos(frequency*np.pi*(X[i]/100))+(Y[j]/100)*(Y[j]/100)-Amplitude*np.cos(frequency*np.pi*(Y[j]/100))
                
            if (obj == 'Rastrigin_hi'):        
                Amplitude = 200
                frequency = 0.35       
                Z[i][j] = Amplitude*2+ (X[i]/100)*(X[i]/100)-Amplitude*np.cos(frequency*np.pi*(X[i]/100))+(Y[j]/100)*(Y[j]/100)-Amplitude*np.cos(frequency*np.pi*(Y[j]/100))
            
            #Ackley Function
            if (obj == 'Ackley'):        
                a =20
                b = 0.2
                c = 2*np.pi
                Z[i][j] = -a * np.exp(-b*np.sqrt(1/2*(X[i]*X[i]+Y[j]*Y[j]))) - np.exp(1/2*(np.cos(c*X[i])+np.cos(c*Y[j]))) + a + np.exp(1)
            
            #Schwefel Function
            if (obj == 'Schwefel'):        
                Z[i][j] = 418.9829*2 - (X[i]/10*np.sin(np.sqrt(np.abs(X[i]/10)))) - (Y[j]/10*np.sin(np.sqrt(np.abs(Y[j]/10))))
           
            #Griewank Function
            if (obj == 'Griewank'):        
                Z[i][j] = (X[i]/1000*X[i]/1000/4000 + Y[j]*0.1*Y[j]/1000/4000) - (np.cos(X[i]/1000)*(np.cos(Y[j]/1000/(np.sqrt(2))))) +1
                
            #Styblinski-tang Function
            if (obj == 'Styblinski-Tang'):        
                Z[i][j] = 1/2 * (X[i]*X[i]*X[i]*X[i] - 16*X[i]*X[i] + 5*X[i]) + (Y[j]*Y[j]*Y[j]*Y[j] -16*Y[j]*Y[j] +5*Y[j])
                
    X, Y = np.meshgrid(X, Y)

    #contour
    fig2 = plt.figure()                                                               
    ax2 = fig2.add_subplot(1,1,1)
    plt.scatter(v1,v2)
    CS = plt.contour(X, Y, Z)
    plt.clabel(CS, inline=1, fontsize=10)
    #major_ticks = np.arange(lower, upper, 50)                                              
    #ax.set_xticks(major_ticks)                                                       
    #ax.set_yticks(major_ticks)                                                       
    ax2.grid(which='major', alpha=0.5)
    
    plt.show()
    plt.savefig('run\\final_lbests.png',dpi=100)