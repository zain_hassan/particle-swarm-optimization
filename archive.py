# -*- coding: utf-8 -*-
"""
Honors' Project
    "Combinatorial Multimodal Optimization using Artificial Life Algorithms"

    Supervisor: Gesche Fensder, Professorship of Computational Mechanics, Technical University of Munich
    Author:     Zain Hassan
    Date:       3 March 2017
    
Species-based Fitness Sharing Combinatorial Particle Swarm Optimization for multimodal optimization(FS-SpeicesCPSO):
    -Combinatorial PSO adapted from 'Combinatorial PSO for partitional clustering problem, Jarboui et al (2007), Elsevier'
    -Species PSO adapted from 'Adaptively Choosing Neighbourhood Bests Using Species in a Particle Swarm Optimizer for Multimodal Function Optimization, Xiaodong Li (2004), Springer'
    -Fitness sharing adapted from 'PSO with sharing for Multimodal function optimization, Li et al (2003)'
    -Inertia reduction idea taken from 'A Dynamic Inertia weight PSO algorithm, Jiao et al (2006), Elsevier'
    -repair, getERP and plotmydesign function adapted from Differential Evolution algorithm written by Gesche Fender  as a part of PhD thesis at Technical University of Munich    

Archive class
"""
import numpy as np
import math
import matplotlib.pyplot as plt

class archive:
    
    @classmethod    
    def initialize_global_variables(cls, dimension,swarm_size, maximize):
        archive.dimension = dimension
        archive.swarm_size = swarm_size
        archive.gbest = np.zeros(archive.dimension)
        archive.gbest_fitness = float('NaN')
        archive.maximize = maximize
    
    @classmethod
    def update_gbest(cls, particle):
        position = particle.position 
        fitness = particle.original_fitness
        
        if math.isnan(archive.gbest_fitness):
            archive.gbest_fitness = fitness
            archive.gbest = np.copy(position)
        elif isbetter(fitness,archive.gbest_fitness,archive.maximize):
            archive.gbest_fitness = fitness
            archive.gbest = np.copy(position)
        else:
            pass
       
    def __init__(self):        
        self.swarm = np.empty((archive.swarm_size,archive.dimension),dtype=int)
        self.fitness = np.empty((archive.swarm_size),dtype=float)

        self.pbest_fitness_original = np.empty((archive.swarm_size),dtype=float)
        self.pbest_fitness_shared = np.empty((archive.swarm_size),dtype=float)

        self.gbest = np.empty((archive.dimension),dtype=int)
        self.gbest_fitness = np.empty((1),dtype=float)
        self.number_of_function_calls = int(0)
        
    def store(self,particle):
        self.store_position(particle.ID, particle.position)
        self.store_fitness(particle.ID,particle.original_fitness)
        self.store_pbest_fitness(particle.ID,
                                 particle.pbest_fitness_shared, 
                                 particle.pbest_fitness_original)
    
    def store_position(self,index,position):
        self.swarm[index][:] = position
        
    def store_fitness(self,index,fitness):
        self.fitness[index] = fitness
        
    def store_pbest_fitness(self,index,pbest_fitness_shared, 
                            pbest_fitness_original):
        self.pbest_fitness_shared[index] = pbest_fitness_shared
        self.pbest_fitness_original[index] = pbest_fitness_original
        
    def store_gbest_fitness(self,gbest_fitness,number_of_function_calls=0):
        self.gbest_fitness = gbest_fitness
        self.number_of_function_calls = number_of_function_calls
        
    def store_gbest(self,gbest):
        self.gbest = gbest
     
    @classmethod
    def print_plot_optimum(cls,fo,rows,columns):       
        archive.print_optimum(fo)        
        plotmydesign(archive.gbest,rows,columns,
                    'Optimum, fitness = '+
                    str(archive.gbest_fitness))                                
    @classmethod
    def print_optimum(cls,fo):
        print_and_save(fo,'\n---------------------------------------\nOptimum design: '+str(archive.gbest)+
                    ' Optimum fitness '+str(archive.gbest_fitness))
        
    @classmethod
    def print_gbest(cls,fo):
        print_and_save(fo,'\n---------------------------------------\ngbest: '+str(archive.gbest)+
                    ' gbest_fitness '+str(archive.gbest_fitness));
   
#can not import these methods from functions.py, hence defining them again                          
#----------------------------------------------
def isbetter(a,b,maximize):
    if (maximize):        
        if(a>b):
            return True
        if(a<b):
            return False

    elif (not maximize):
        if(a>b):
            return False
        if(a<b):
            return True
    
    else:
        return False
        
def print_and_save(fo,string):
    fo.write(string);
    print(string)
    
#--------------------------    
def plotmydesign(design,rows=4,columns=8,title='No Title'):
    xlist = []
    ylist = []    
    for m in range (columns+1):
        xlist.append(m)        
    for n in range (rows+1):
        ylist.append(n)
        
    plt.figure()
    plt.xlim(0,rows)
    plt.ylim(0,columns)
    plt.xticks(xlist)
    plt.yticks(ylist)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.grid(True, color='w', linestyle='-', linewidth=2)
    plt.gca().patch.set_facecolor('0.8')
    plt.gca().axes.xaxis.set_ticklabels([])
    plt.gca().axes.yaxis.set_ticklabels([])
    plt.text(0,-0.5,title)
    for d in design:
        i = int(((d-1)%columns)+1)
        j = int(((d-i-1)/columns)+2)
        plt.text(i-0.5, j-0.5, "io", size=30, rotation=0.,ha="center", 
                 va="center",bbox=dict(boxstyle="square",facecolor='k'))                             
    plt.savefig('run\\'+title+'.png',dpi=100)
    plt.show()