# -*- coding: utf-8 -*-
"""
Honors' Project
    "Combinatorial Multimodal Optimization using Artificial Life Algorithms"

    Supervisor: Gesche Fensder, Professorship of Computational Mechanics, Technical University of Munich
    Author:     Zain Hassan
    Date:       3 March 2017
    
Species-based Combinatorial Particle Swarm Optimization (SpeicesCPSO):
    -CPSO adapted from 'Combinatorial PSO for partitional clustering problem, Jarboui et al (2007), Elsevier'
    -SPSO adapted from 'Adaptively Choosing Neighbourhood Bests Using Species in a Particle Swarm Optimizer for Multimodal Function Optimization, Xiaodong Li (2004), Springer'
    -repair function and plotmydesign function adapted from Differential Evolution algorithm written by Gesche Fender's  as a part of PhD thesis at Technical University of Munich
    
Functions
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import time
from classes import archive
#from mpl_toolkits.mplot3d import Axes3D
#from matplotlib.ticker import LinearLocator, FormatStrFormatter

def isbetter(a,b,maximize):
    if (maximize):        
        if(a>b):
            return True
        if(a<b):
            return False

    elif (not maximize):
        if(a>b):
            return False
        if(a<b):
            return True
    
    else:
        return False

#'''
def dist(vector1,vector2):
    if len(vector1)!=len(vector2):
        return 0.
    else:
        result = 0.
        vector1 = np.sort(vector1)
        vector2 = np.sort(vector2)        
        for i in vector1:
            for j in vector2:
                result += ((i-j)*(i-j))
        return np.sqrt(result)
#'''     
'''
#higher dimension norm   
def dist(vector1,vector2):
    if len(vector1)!=len(vector2):
        return 0.
    else:
        n = len(vector1)
        result = 0.
        vector1 = np.sort(vector1)
        vector2 = np.sort(vector2)        
        for i in vector1:
            for j in vector2:
                result += np.abs(np.power((i-j),n))
        return np.power(result,1/n)
'''        
def divide_into_subswarms(swarm_size,subswarm_radius,min_no_subswarms, swarm, maximize):
    radius = np.copy(subswarm_radius)    
    subswarm_ID_list = set_subswarm_ID(swarm_size,radius, swarm,maximize)    
    counter = 0
    max_counter = 200
    while(len(subswarm_ID_list)<min_no_subswarms and counter<max_counter):
        if (len(subswarm_ID_list)==1 and subswarm_ID_list[0]==0):
            radius = radius*1.15            
        else:
            radius = radius*0.99
        for i in range (len(swarm)):
            swarm[i].subswarm_ID = -1
            swarm[i].isseed = False
        subswarm_ID_list = set_subswarm_ID(swarm_size,radius, swarm, maximize)
        counter +=1
        if (counter==100):
            #min_no_subswarms -=1
            radius = radius*5
            
        if(counter==max_counter):
            print("WARNING!: Failed to make desired number of subswarms")            
            time.sleep(2)
    
    #equalize_subswarms()
    print("//////////////////////////////////////////////////")
    subswarm = [[] for n in range(len(subswarm_ID_list))]
    for it_2 in range(len(subswarm_ID_list)):
        for it_3 in range(swarm_size):
            if swarm[it_3].subswarm_ID == subswarm_ID_list[it_2]:               
                subswarm[it_2].append(swarm[it_3])
    return subswarm
    
def set_subswarm_ID (swarm_size,subswarm_radius, swarm,maximize):
    
    #make a sorted list of all particel IDs and fitnesses (ascending wrt fitness)
    sorted_list = np.zeros((swarm_size,2))
    for it_1 in range(swarm_size):
        sorted_list[it_1,0] = np.copy(swarm[it_1].ID)
        sorted_list[it_1,1] = np.copy(swarm[it_1].fitness)            
        #sorted_list[it_1,1] = np.copy(swarm[it_1].pbest_fitness)    
    sorted_list = sorted_list[sorted_list[:,1].argsort()]
    
    #Make subswarm seeds and assign subswarm groups
    if (maximize):
        for it_2 in range(swarm_size-1,-1,-1):
            found = False
            for it_3 in range (swarm_size):
                #assign to a subswarm        
                if (swarm[it_3].isseed == True and dist(swarm[sorted_list[it_2,0]].position,swarm[it_3].position)<subswarm_radius):
                    found = True
                    swarm[sorted_list[it_2,0]].subswarm_ID = swarm[it_3].subswarm_ID
                    #swarm[sorted_list[it_2,0]].set_lbest(swarm[it_3].position,swarm[it_3].fitness)
                    break
                else:    
                    pass
            #make a seed
            if found == False:
                swarm[sorted_list[it_2,0]].isseed = True
                swarm[sorted_list[it_2,0]].subswarm_ID = swarm[sorted_list[it_2,0]].ID+1
                #swarm[sorted_list[it_2,0]].set_lbest(swarm[it_3].pbest,swarm[it_3].pbest_fitness)
                
    if (not maximize):
        for it_2 in range(swarm_size):
            found = False
            for it_3 in range (swarm_size):
                #assign to a subswarm        
                if (swarm[it_3].isseed == True and dist(swarm[sorted_list[it_2,0]].position,swarm[it_3].position)<subswarm_radius):
                    found = True
                    swarm[sorted_list[it_2,0]].subswarm_ID = swarm[it_3].subswarm_ID
                    #swarm[sorted_list[it_2,0]].set_lbest(swarm[it_3].position,swarm[it_3].fitness)
                    break
                else:    
                    pass
            #make a seed
            if found == False:
                swarm[sorted_list[it_2,0]].isseed = True
                swarm[sorted_list[it_2,0]].subswarm_ID = swarm[sorted_list[it_2,0]].ID+1
                #swarm[sorted_list[it_2,0]].set_lbest(swarm[it_3].pbest,swarm[it_3].pbest_fitness)
        
    #Put all single seeds into one subswarm
    for it_4 in range (swarm_size):
        single = True
        for it_5 in range (swarm_size):
            if swarm[it_4].subswarm_ID == swarm[it_5].subswarm_ID and it_4!=it_5:
                single = False
            else:
                pass
        if single:
            swarm[it_4].subswarm_ID = 0
            swarm[it_4].isseed = False
   
    #make a list of all subswarm_IDs of seeds
    subswarm_IDs = np.empty(swarm_size,dtype=int)    
    for i in range (swarm_size):
        subswarm_IDs[i] = swarm[i].subswarm_ID
    subswarm_IDs = np.unique(np.sort(subswarm_IDs))
    
    print("New subswarms created with IDs: ",subswarm_IDs)
    
    return subswarm_IDs

#--------------------------    
def assemble_main_swarm(swarm_size,subswarm):
    swarm = np.empty((swarm_size), dtype=object)
    for i in range(len(subswarm)):    
        for j in range(np.size(subswarm[i])):
            swarm[subswarm[i][j].ID] = subswarm[i][j]
            swarm[subswarm[i][j].ID].subswarm_ID = -1
            swarm[subswarm[i][j].ID].isseed = False
    return swarm
    
#--------------------------
def update_lbest(subswarm,maximize):
    for i in range(len(subswarm)):
        fitness = np.copy(subswarm[i][0].pbest_fitness)
        index = 0
        for j in range(np.size(subswarm[i])-1):
            if isbetter(subswarm[i][j+1].pbest_fitness,fitness,maximize):
                fitness = subswarm[i][j+1].pbest_fitness
                index = j+1
        for k in range(np.size(subswarm[i])):
            subswarm[i][k].lbest_fitness = fitness
            subswarm[i][k].lbest = subswarm[i][index].pbest
   
#-------------------------
def write_header_to_file(fo,swarm_size,max_gen,subswarm_radius,subswarm_frequency,inertia,c_p,c_g,alpha,epsilon,adaptive_c_p,rows,columns,min_no_subswarms):
    #output = ()    
    
    fo.write('Problem size: '+str(rows)+'x'+str(columns)+
    '\nMinimum no. of subswarms: '+str(min_no_subswarms)+
    '\nswarm size: '+str(swarm_size)+'\nmaximum generations: '+str(max_gen)+
    '\nInitial Radius of subswarm: '+str(subswarm_radius)+
    '\nFrequency of subswarm creation: '+str(subswarm_frequency)+
    '\nInertia: '+str(inertia)+
    '\nCoefficient for personal best: '+str(c_p)+
    '\nCoefficient for gbest/lbest: '+str(c_g)+
    '\nAlpha for CPSO: '+str(alpha)+
    '\nTolerance for optima: '+str(epsilon)+
    '\nIs Coefficient for personal best adaptive?: '+str(adaptive_c_p)+
    '\n\n');     
 #-------------------------
def print_and_save(fo,string):
    fo.write(string);
    print(string)
    
#--------------------------             
def plot_pbest_fitness(log):
    X = np.arange(1.,float(len(log)+1),1.)
    fig = plt.figure()   
    ax=fig.add_subplot(111)
    Y = np.empty((len(log)))
    for i in range (archive.swarm_size):
        for j in range ((len(log))):
            Y[j] = log[j].pbest_fitness[i]
        line, = ax.plot(X,Y)
    ax.set_xlabel('generation')
    ax.set_ylabel('pbest fitness of all particles')
    title='pbest of all particles over generations'
    ax.set_title(title)
    plt.savefig('run\\'+title+'.svg')
    plt.show()        

#-------------------------    
def plot_gbest_fitness(log):
    X = np.arange(1.,float(len(log)+1),1.)
    fig = plt.figure()   
    ax=fig.add_subplot(111)
    Y = np.empty((len(log)))
    for i in range ((len(log))):
            Y[i] = log[i].gbest_fitness
    line, = ax.plot(X,Y)
    ax.set_xlabel('generation')
    ax.set_ylabel('gbest fitness')
    title='gbest of swarm over generations'
    ax.set_title(title)
    plt.savefig('run\\'+title+'.svg')    
    plt.show()

#--------------------------    
def plot_particle_history(log,n=0):
    X = np.arange(1.,float(len(log)+1),1.)
    fig = plt.figure()   
    ax=fig.add_subplot(111)
    Y = np.empty((len(log)))
    Z = np.empty((len(log)))
    for i in range ((len(log))):
        Y[i] = log[i].fitness[n]
        Z[i] = log[i].pbest_fitness[n]
    line, = ax.plot(X,Y)
    line, = ax.plot(X,Z,linewidth = 2)
    ax.set_xlabel('generation')
    ax.set_ylabel('fitness')
    title = 'fitness of particle '+str(n)+' over generations'
    ax.set_title(title)
    plt.savefig('run\\'+title+'.svg')
    plt.show()
   
#--------------------------    
def plotmydesign(design,rows=10,columns=10,title='No Title'):
    xlist = []
    ylist = []    
    for m in range (columns+1):
        xlist.append(m)        
    for n in range (rows+1):
        ylist.append(n)
        
    plt.figure()
    plt.xlim(0,rows)
    plt.ylim(0,columns)
    plt.xticks(xlist)
    plt.yticks(ylist)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.grid(True, color='w', linestyle='-', linewidth=2)
    plt.gca().patch.set_facecolor('0.8')
    plt.gca().axes.xaxis.set_ticklabels([])
    plt.gca().axes.yaxis.set_ticklabels([])
    plt.text(0,-0.5,title)
    for d in design:
        i = ((d-1)%columns)+1
        j = ((d-i)/columns)+1
        plt.text(i-0.5, j-0.5, "io", size=30, rotation=0.,ha="center", va="center",bbox=dict(boxstyle="square",facecolor='k'))                             
    plt.savefig('run\\'+title+'.svg')
    #plt.close()
    plt.show()


#--------------------------
def print_all_optima(log,fo,epsilon):
    n = len(log[0].swarm[0,:])    
    memory = np.empty((n,1))   
    memory[:,0] = archive.gbest
    counter=0
    for it_2 in range(len(log)):
        for it_4 in range (len(log[0].swarm[:,0])):
            if(abs(log[it_2].fitness[it_4]-archive.gbest_fitness)<=epsilon):
                new = True
                for it_3 in range (len(memory[0,:])):
                    if (np.sum(np.square(log[it_2].swarm[it_4,:]-memory[:,it_3]))==0):
                        new = False
                if new:
                    counter+=1                    
                    temp = np.empty((n,1))
                    temp[:,0] = log[it_2].swarm[it_4,:]
                    memory = np.append(memory,temp,axis=1)
                    print_and_save(fo,'\noptimum#'+str(counter)+' position '+str(log[it_2].swarm[it_4,:])+' fitness '+str(log[it_2].fitness[it_4]))    
    print_and_save(fo,'\nMULTIMODAL: Number of optima within +-'+str(epsilon)+' of global optimum '+str(archive.gbest_fitness)+' are '+str(counter))
    
    
'''
#--------------------------

def plot_swarm_fitness(log):
    fig, ax = plt.subplots(subplot_kw={'projection': '3d'})
    X = np.arange(1.,float(len(log)+1),1.)
    for i in range (archive.swarm_size):
        for j in range ((len(log))):
            Y[j] = log[j].fitness[i]
        ax.plot(X,Y,i+1)
        ax.set_xlabel('generations')
        ax.set_ylabel('fitness')
        ax.set_zlabel('particle number')
        ax.set_title('Fitness of all particles vs. generation')
    plt.show()  
    
#--------------------------
def plot_2D(vector):
    
    length = np.size(vector)
    X = np.arange(1.,float(length+1),1.)
    
    fig2 = plt.figure()   
    ax2=fig2.add_subplot(111)
    line, = ax2.plot(X,vector)
    plt.show()
    
    return 0
    
#--------------------------
def plot_multiple2D(matrix,multiple=True):
    
    rows, columns = matrix.shape
    fig1, ax1 = plt.subplots(subplot_kw={'projection': '3d'})
    X = np.arange(1.,float(columns+1),1.)
    if multiple==True:
        for i in range (rows):
            ax1.plot(X,matrix[i,:],i+1)
    if multiple==False:
        for i in range (rows):
            ax1.plot(X,matrix[i,:])
   
    plt.show()
    
    return 0
    
    
#--------------------------
def plot_3D(matrix):
    
    rows, columns = matrix.shape

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    X = np.arange(1.,float(columns+1),1.)
    Y = np.arange(1.,float(rows+1),1.)
    X, Y = np.meshgrid(X, Y)
    surf = ax.plot_surface(X, Y, matrix,  linewidth=0, antialiased=True)
    
    # Customize the z axis.
    ax.set_zlim(0., 1.)
    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    
    # Add a color bar which maps values to colors.
    fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()
    return 0
'''

#--------------------------
def plot_points(subswarm,g):
    plt.clf()    
    colors = iter(cm.rainbow(np.linspace(0, 1, len(subswarm))))
    for i in range(len(subswarm)):    
        X=[]
        Y=[]
        for j in range(np.size(subswarm[i])):
            X.append(subswarm[i][j].position[0])      
            Y.append(subswarm[i][j].position[1])      
        plt.scatter(X,Y,color=next(colors))
    plt.savefig('run\\swarm'+str(g)+'.png')
    

#--------------------------
def plot_lbests(v1,v2):
    Amplitude = 200
    frequency = 0.35
    X = np.arange(1,25,.1)
    Y = np.arange(1,25,.1)
    Z = np.empty((len(X),len(Y)))
    for i in range(len(X)):
        for j in range(len(Y)):
            Z[i][j] = Amplitude*2+ (X[i]+2)*(X[i]+2)-Amplitude*np.cos(frequency*np.pi*(X[i]+2))+(Y[j]+2)*(Y[j]+2)-Amplitude*np.cos(frequency*np.pi*(Y[j]+2))
                
    X, Y = np.meshgrid(X, Y)
    
    '''
    fig2 = plt.figure()
    ax2 = fig2.gca(projection='3d')
    surf2 = ax2.plot_surface(X, Y, Z,  linewidth=0, antialiased=True)
    '''
    
    #'''
    fig = plt.figure()                                                               
    ax = fig.add_subplot(1,1,1)
    plt.scatter(v1,v2)
    CS = plt.contour(X, Y, Z)
    plt.clabel(CS, inline=1, fontsize=10)
    major_ticks = np.arange(0, 25, 1)                                              
    ax.set_xticks(major_ticks)                                                       
    ax.set_yticks(major_ticks)                                                       
    ax.grid(which='major', alpha=0.5)
    #'''
    
    plt.show()