from abaqus import *
from abaqusConstants import *
from odbAccess import *
from textRepr import *
import numpy as np
#import sys

Z_Luft=413.5 #rho times c at 20 degree Celsius
numbersteps=2
elementsize=(0.5*0.5)      #elementsize in squaremeters
normalDirection=2    # [0]=u1, [1]=u2, [2]=u3
    
print ('Starting ERP Calculation')

myodb=openOdb(path='d:/FGCM/Zain_Hassan/SpeciesCPSO/input_temp.odb')
step1=myodb.steps.values()[0]

ERP=[]
frequency=[]

for framenumber in range(numbersteps):
    displacement= myodb.steps['Harmonic Analysis'].frames[framenumber].fieldOutputs['U']
    frequency_temp=myodb.steps['Harmonic Analysis'].frames[framenumber].frequency
    
    #set lowerpanel is not all nodes of lower panel!
    panel = myodb.rootAssembly.nodeSets['NODESPANEL']
    
    uNormal=[]
    
    panelDisplacement = displacement.getSubset(region=panel)
    panelValues = panelDisplacement.values
    for v in panelValues:
        uNormal.append(np.square(v.data[normalDirection]))   # [0]=u1, [1]=u2, [2]=u3
          
    uNormalsum_temp=np.sum(uNormal)
    #weight is 4*0.25*elementsize=elementsize for linear
    #weight for quadratic: field output set to "external" in abaqus, thus same as linear
    ERP_temp=np.square(2*np.pi*frequency_temp)*Z_Luft*uNormalsum_temp*elementsize
    #print u2sum
    ERP.append(ERP_temp)
    frequency.append(frequency_temp)

myodb.close()

data=[frequency, ERP]
print (data)

f = open("d:/FGCM/Zain_Hassan/SpeciesCPSO/ERP_temp.txt", "w")
f.write(str(data))
f.close()

print ('done computing ERP ' )
