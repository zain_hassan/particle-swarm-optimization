# -*- coding: utf-8 -*-
"""
Honors' Project
    "Combinatorial Multimodal Optimization using Artificial Life Algorithms"

    Supervisor: Gesche Fensder, Professorship of Computational Mechanics, Technical University of Munich
    Author:     Zain Hassan
    Date:       3 March 2017
    
Fitness Sharing Combinatorial Particle Swarm Optimization for multimodal optimization(FS-SpeicesCPSO):
    -Combinatorial PSO adapted from 'Combinatorial PSO for partitional clustering problem, Jarboui et al (2007), Elsevier'
    -Fitness sharing adapted from 'PSO with sharing for Multimodal function optimization, Li et al (2003)'
    -Inertia reduction idea taken from 'A Dynamic Inertia weight PSO algorithm, Jiao et al (2006), Elsevier'
    -repair, getERP and plotmydesign function adapted from Differential Evolution algorithm written by Gesche Fender  as a part of PhD thesis at Technical University of Munich    
    -Test functions taken from 'https://www.sfu.ca/~ssurjano/optimization.html' (accessed on: 06 April 2017)
    
Classes
"""
import numpy as np
import random, math

class particle:
    
    @classmethod
    def initialize_global_variables(cls, length,upper_bound,lower_bound, maximize, obj):
        particle.length = length
        particle.lower_bound = lower_bound
        particle.upper_bound = upper_bound
        particle.gbest = np.zeros(length)
        particle.gbest_fitness = float('NaN')
        particle.maximize = maximize
        particle.obj = obj
        
    @classmethod 
    def set_inertia(cls, inertia=0.9):
        particle.inertia = inertia
        
    @classmethod    
    def set_c_p(cls, c_p=0.5):
        particle.c_p = c_p
        
    @classmethod    
    def set_c_g(cls, c_g=0.5):
        particle.c_g = c_g
        
    @classmethod    
    def set_alpha(cls, alpha=0.5):
        particle.alpha = alpha

    @classmethod
    def update_gbest(cls,position,fitness):
        if (math.isnan(particle.gbest_fitness)):
            particle.gbest_fitness = fitness
            particle.gbest = np.copy(position)
        elif isbetter(fitness,particle.gbest_fitness,particle.maximize):
            particle.gbest_fitness = fitness
            particle.gbest = np.copy(position)
        else:
            pass
        return particle.gbest, particle.gbest_fitness

    def __init__(self,ID):    
        self.ID = int(ID)
        
        self.position = np.empty(particle.length,dtype=int)
        for i in range (particle.length):
            self.position[i] =  random.randrange(particle.lower_bound,particle.upper_bound,1)
        self.repair()
        
        self.fitness = self.evaluate_fitness()
        
        self.pbest = np.copy(self.position)
        self.pbest_fitness = np.copy(self.fitness)
        
        self.transformed_position = self.transform()
        
        self.velocity = np.zeros(particle.length)
        for i in range (particle.length):                  #initialization with a random number 
            self.velocity[i] = 1-2*random.random()         #between -1 and 1         
    
    #main SOLVE function            
    def move_and_update(self):
        self.transform()                                    #transform the position into space of (-1,0,1)
        self.update_velocity()                              #update velocity based on pbest and gbest
        self.update_transformed_position()                  #get new transformed position from new velocity
        self.inverse_transform()                            #transform the position back to integer space
        self.repair()                                       #check for repititions in position and replace them with randoml numbers
        self.evaluate_fitness()                             #evaluate fitness of the new position 
        self.update_pbest()                                 #update the local best
        
    #transforming the design vector        
    def transform (self):

        transformed_position = np.zeros(particle.length)        

        for i in range (particle.length):
            if self.position[i] == particle.gbest[i] and self.position[i] == self.pbest[i]:             
                transformed_position[i]= random.randrange(-1,2,1)
            elif self.position[i] == particle.gbest[i]:
                transformed_position[i]=int(1)
            elif self.position[i] == self.pbest[i]:
                transformed_position[i]=int(-1)
            else:
                transformed_position[i] = 0
        self.transformed_position = transformed_position
        return transformed_position
        
    #transforming back to design vector        
    def inverse_transform (self):
        
        position = np.zeros(particle.length)
        
        for i in range (particle.length):
            if self.transformed_position[i] == int(1):
                position[i] = particle.gbest[i]
            elif self.transformed_position[i] == int(-1):
                position[i] = self.pbest[i]
            else:
                position[i] = random.randint(particle.lower_bound,particle.upper_bound)
        self.position = position
        return position
        
    #repair function adapted from Gesche's Differential Evolution code
    def repair(self):
        bucket = np.arange(particle.lower_bound,(particle.upper_bound)+1,1)             #bucket is a set of all feasible solution components
        self.position = np.unique(self.position)                                        #remove duplications
        for k in self.position:
            if k < particle.lower_bound or k > particle.upper_bound:
                self.position = np.delete(self.position,np.argwhere(self.position==k))  #remove out-of-bound entries
        for i in self.position:
            bucket = np.delete(bucket,np.argwhere(bucket==i))                                            #remove elements from bucket that are already in position vector to avoid repitition
        for j in range (particle.length - np.size(self.position)):                      #loop till position vector is same length as standard
            r = np.random.choice(bucket)
            self.position = np.append(self.position,r)                                  #pick a random from bucket and append to position
            bucket = np.delete(bucket,np.argwhere(bucket==r))                           #remove the already picked element from bucket to avoid repitition
            
        self.position = np.sort(self.position)            
        return self.position
        
    def update_velocity(self):
        r_p = random.random()
        r_g = random.random()
        
        for i in range (particle.length):
            self.velocity[i] = self.inertia*self.velocity[i]+r_p*self.c_p*(-1-self.transformed_position[i])+r_g*self.c_g*(1-self.transformed_position[i])
        return self.velocity
        
    def update_transformed_position(self):
        for i in range (particle.length):
            temp = self.transformed_position[i]+self.velocity[i]        
            if temp > self.alpha:
                self.transformed_position[i] = int(1)
            elif temp < -1*(self.alpha):
                self.transformed_position[i] = int(-1)
            else:
                self.transformed_position[i] = int(0)
        return self.transformed_position 
       
    def update_pbest(self):
        if isbetter(self.fitness,self.pbest_fitness,particle.maximize):
            self.pbest_fitness = np.copy(self.fitness)
            self.pbest = np.copy(self.position)
        return self.pbest, self.pbest_fitness
        
    def evaluate_fitness(self):
        #simple addition (unimodal)        
        if (particle.obj == 'simple_addition'):        
            fitness = 0
            for i in range (np.size(self.position)):
                fitness += self.position[i]
            self.fitness = fitness
            return fitness

        #Two peak trap (multimodal) as defined in
        #D. Beasley, D. R. Bull, Ralph R. Martin. (1993). A sequential Niche Technique for Multimodal Function Optimization 
        #Originally defined by    
        #Ackley, D. H. (1987). An emperical study of bit vector function optimization. In L. Davis (Ed.), Genetic Algorithms and Stimulated Annealing (Ch. 13). Pitman
        if (particle.obj == '2-peaks'):        
            evens = 0    
            temp = np.copy(self.position) 
            temp = temp%2
            for i in temp:
                if i ==0:
                    evens+=1
                else:
                    pass
            z = particle.length/2
            
            if evens < z:
                self.fitness =  180/z*(z-evens) #odd peak
            else:
                self.fitness = 200/z*(evens-z)  #even peak
            return self.fitness

        #Simplified (Modified) Rastrigin Function
        if (particle.obj == 'Simplified_Rastrigin'):        
            evens = 0
            p = 2
            frequency = 0.5     
            temp = np.copy(self.position)
            temp = temp%2
            for i in temp:
                if i ==0:
                    evens+=1
                else:
                    pass
            self.fitness = 25-(0.1*((evens-p)*(evens-p))-10*np.cos(frequency*np.pi*(evens-p)+2))
            return self.fitness
    
        #Sphere function
        if (particle.obj == 'Sphere'):        
            self.fitness = 0
            for x in self.position:
                self.fitness += x*x
            return self.fitness
    
        #Rastrigin Function
        if (particle.obj == 'Rastrigin_lo'):        
            Amplitude = 200
            frequency = 0.17
            d = np.size(self.position)
            self.original_fitness = Amplitude*d
            for x in self.position:
                self.original_fitness += (x/100)*(x/100)-Amplitude*np.cos(frequency*np.pi*(x/100))
            return self.original_fitness
            
        if (particle.obj == 'Rastrigin_hi'):        
            Amplitude = 200
            frequency = 0.35
            d = np.size(self.position)
            self.original_fitness = Amplitude*d
            for x in self.position:
                self.original_fitness += (x/100)*(x/100)-Amplitude*np.cos(frequency*np.pi*(x/100))
            return self.original_fitness
    
        #Ackley Function
        if (particle.obj == 'Ackley'):        
            a=20
            b=0.2
            c=2*np.pi
            d=len(self.position)
            p1 = 0
            p2 = 0
            for x in self.position:
                p1 += x*x
                p2 += np.cos(c*x)
            self.fitness = -a*np.exp(-b*np.sqrt(1/d*p1))-np.exp(1/d*p2) + a+np.exp(1)
            return self.fitness
    
        #Schwefel Function
        if (particle.obj == 'Schwefel'):        
            d=len(self.position)
            p1= 0
            for x in self.position:
                p1 += (x/10 * np.sin(np.sqrt(np.abs(x/10))))
            self.fitness = 418.9829 * d - p1
            return self.fitness
    
        #Griewank Function
        if (particle.obj == 'Griewank'):        
            p1 = 0
            p2 = 0
            for i in range(len(self.position)): 
                p1 += (self.position[i]/1000*self.position[i]/1000)/4000
                p2 *= np.cos(self.position[i]/1000/np.sqrt(i+1))
            self.fitness = p1-p2+1
            return self.fitness
        
        #Styblinski-tang Function
        if (particle.obj == 'Styblinski-Tang'):        
            self.fitness = 0
            for x in self.position:
                self.fitness += (x*x*x*x - 16*x*x +5*x)
            self.fitness *= 1/2
            return self.fitness
            
        #reference for test functions: https://www.sfu.ca/~ssurjano/optimization.html
    
#----------------------------------------------
def isbetter(a,b,maximize):
    if (maximize):        
        if(a>b):
            return True
        if(a<b):
            return False

    elif (not maximize):
        if(a>b):
            return False
        if(a<b):
            return True
    
    else:
        return False