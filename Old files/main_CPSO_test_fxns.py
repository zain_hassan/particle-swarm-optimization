# -*- coding: utf-8 -*-
"""
Honors' Project
    "Combinatorial Multimodal Optimization using Artificial Life Algorithms"

    Supervisor: Gesche Fensder, Professorship of Computational Mechanics, Technical University of Munich
    Author:     Zain Hassan
    Date:       3 March 2017
    
Species-based Fitness Sharing Combinatorial Particle Swarm Optimization for multimodal optimization(FS-SpeicesCPSO):
    -Combinatorial PSO adapted from 'Combinatorial PSO for partitional clustering problem, Jarboui et al (2007), Elsevier'
    -Species PSO adapted from 'Adaptively Choosing Neighbourhood Bests Using Species in a Particle Swarm Optimizer for Multimodal Function Optimization, Xiaodong Li (2004), Springer'
    -Fitness sharing adapted from 'PSO with sharing for Multimodal function optimization, Li et al (2003)'
    -Inertia reduction idea taken from 'A Dynamic Inertia weight PSO algorithm, Jiao et al (2006), Elsevier'
    -repair, getERP and plotmydesign function adapted from Differential Evolution algorithm written by Gesche Fender  as a part of PhD thesis at Technical University of Munich    
    -Test functions taken from 'https://www.sfu.ca/~ssurjano/optimization.html' (accessed on: 06 April 2017)
    
Main File
"""
import numpy as np
from classes_CPSO_test_fxns import particle
from archive import archive
from functions import write_header_to_file_CPSO, print_and_save, print_all_optima
from functions import plot_pbest_fitness, plot_gbest_fitness, plot_particle_history

#************INITIALIZATION************

maximize = False
minimize = not maximize

rows = int(2)
columns = int(1)
upper_bound = 5000                   
lower_bound = -5000
range_ = upper_bound-lower_bound
dimension = 2

#obj_fxn = 'Ackley'
#obj_fxn = 'Rastrigin'
#obj_fxn = 'Griewank'
obj_fxn = 'Schwefel'
#obj_fxn = 'Sphere'
#obj_fxn = 'Styblinski-Tang'
#obj_fxn = '2-peaks'
#obj_fxn = 'Simplified_Rastrigin'
#obj_fxn = 'simple_addition'
#'''

#set parameters of CPSO
swarm_size = 25                                                               #swarm size = number of particles (default: equal to or greater than range) 
max_gen = 100                                                                 #maximum number of generations
inertia = 0.85         #0.85                                                  #inertia for velocity
c_p = 0.6              #0.6                                                   #coefficient for cognitive behaviour
c_g = 0.4              #0.4                                       	          #coefficient for social behaviour
alpha = 7/swarm_size   #0.35                                                  #factor for inverse transformation in CPSO
epsilon = 100                                                                 #+-epsilon around global optimum also considered as global optima
adaptive_c_p = False                                                          #adaptively decrease c_p in every generation to force convergence

particle.set_inertia(inertia)
particle.set_c_p(c_p ) 
particle.set_c_g(c_g) 
particle.set_alpha(alpha)

#file for writing results
fo = open("run\\result.txt", "w")
write_header_to_file_CPSO(fo,swarm_size,max_gen,inertia,c_p,c_g,alpha,epsilon,adaptive_c_p,rows,columns)
  
#initialize first main swarm and global variables
particle.initialize_global_variables(dimension,upper_bound,lower_bound,maximize,obj_fxn)
swarm = np.empty((swarm_size), dtype=object)
for it_0 in range (swarm_size):
    swarm[it_0]= particle(it_0)

for iterator_2 in range (swarm_size):
    particle.update_gbest(swarm[iterator_2].pbest,swarm[iterator_2].pbest_fitness)

#set up achives
archive.initialize_global_variables(dimension,swarm_size,maximize)
log = np.empty((max_gen),dtype=object)
for it_1 in range (max_gen):
    log[it_1] = archive()

#************SOLUTION************
for g in range(max_gen):                                                       #Loop over generations
    print_and_save(fo,'\n######################## GEN: '+str(g+1))
    
    #adaptive shift from exploration to exploitation, added to improve convergence    
    if (adaptive_c_p ):
        #particle.set_c_p(c_p*(1-1/max_gen*g))                                 #linear 
        particle.set_c_p(c_p*np.exp(-g))                                       #exponential
    else:
        pass
             
    #Loop over the swarm
    for i in range(swarm_size):
           
        #archiving
        log[g].store(swarm[i].ID,
                        swarm[i].position,
                        swarm[i].fitness,
                        swarm[i].pbest_fitness)
        fo.write('\ngen# '+str(g)
        +' particle# '+str(swarm[i].ID)
        +' position: '+str(swarm[i].position)
        +' fitness: '+str(swarm[i].fitness));
        
        particle.update_gbest(swarm[i].position,swarm[i].fitness)
        archive.update_gbest(swarm[i].position,swarm[i].fitness)    #For archiving; update the global best from all particles            
        
        swarm[i].move_and_update()

#************POST-PROCESSING************
    log[g].store_gbest_fitness(archive.gbest_fitness)                          #save the global best at this generation
    print_and_save(fo,
                '\ngbest: '+str(particle.gbest)+
                ' gbest_fitness '+str(particle.gbest_fitness));
    print_and_save(fo,'\n###############################################')
    
print_and_save(fo,'\n###############################################')

print_all_optima(log,fo,epsilon)

#plotting the archive
plot_pbest_fitness(log)
plot_gbest_fitness(log)
plot_particle_history(log,1)

fo.close()