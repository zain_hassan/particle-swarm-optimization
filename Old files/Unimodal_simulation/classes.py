# -*- coding: utf-8 -*-
"""
Honors' Project
    "Combinatorial Multimodal Optimization using Artificial Life Algorithms"

    Supervisor: Gesche Fensder, Professorship of Computational Mechanics, Technical University of Munich
    Author:     Zain Hassan
    Date:       3 March 2017
    
Species-based Combinatorial Particle Swarm Optimization (SpeicesCPSO):
    -CPSO adapted from 'Combinatorial PSO for partitional clustering problem, Jarboui et al (2007), Elsevier'
    -SPSO adapted from 'Adaptively Choosing Neighbourhood Bests Using Species in a Particle Swarm Optimizer for Multimodal Function Optimization, Xiaodong Li (2004), Springer'
    -repair function and plotmydesign function adapted from Differential Evolution algorithm written by Gesche Fender's  as a part of PhD thesis at Technical University of Munich
    
Classes
"""
import numpy as np
import random, math, os

class particle:
    
    @classmethod
    def initialize_global_variables(cls, swarm_size, length,upper_bound,lower_bound, maximize):
        particle.swarm_size = swarm_size
        particle.length = length
        particle.lower_bound = lower_bound
        particle.upper_bound = upper_bound
        particle.gbest = np.zeros(length)
        particle.gbest_fitness = float('NaN')
        particle.maximize = maximize
    
    @classmethod 
    def set_inertia(cls, inertia=0.9):
        particle.inertia = inertia
        
    @classmethod    
    def set_c_p(cls, c_p=0.5):
        particle.c_p = c_p
        
    @classmethod    
    def set_c_g(cls, c_g=0.5):
        particle.c_g = c_g
        
    @classmethod    
    def set_alpha(cls, alpha=0.5):
        particle.alpha = alpha

    def __init__(self,ID):    

        self.ID = int(ID)
        self.subswarm_ID = -1
        self.isseed = False
        
        self.position = np.empty(particle.length,dtype=int)
        for i in range (particle.length):
            self.position[i] =  random.randrange(particle.lower_bound,particle.upper_bound,1)
        self.repair()
        
        self.fitness = self.evaluate_fitness_initial()
        
        self.pbest = np.copy(self.position)
        self.pbest_fitness = np.copy(self.fitness)
        
        self.lbest = np.copy(self.position)
        self.lbest_fitness = np.copy(self.fitness)
        
        self.transformed_position = self.transform()
        
        self.velocity = np.zeros(particle.length)
        for i in range (particle.length):                  #initialization with a random number 
            self.velocity[i] = 1-2*random.random()         #between -1 and 1         
    
    #main SOLVE function            
    #def move_and_update(self):
    def move_and_update(self,log):
        self.transform()                                    #transform the position into space of (-1,0,1)
        self.update_velocity()                              #update velocity based on pbest and gbest
        self.update_transformed_position()                  #get new transformed position from new velocity
        self.inverse_transform()                            #transform the position back to integer space
        self.repair()                                       #check for repititions in position and replace them with randoml numbers
        #self.evaluate_fitness()                             #evaluate fitness of the new position 
        self.evaluate_fitness(log)
        self.update_pbest()                                 #update the local best
        
    #transforming the design vector        
    def transform (self):

        transformed_position = np.zeros(particle.length)        

        for i in range (particle.length):
            if self.position[i] == self.lbest[i] and self.position[i] == self.pbest[i]:             
                transformed_position[i]= random.randrange(-1,2,1)
            elif self.position[i] == self.lbest[i]:
                transformed_position[i]=int(1)
            elif self.position[i] == self.pbest[i]:
                transformed_position[i]=int(-1)
            else:
                transformed_position[i] = 0
        self.transformed_position = transformed_position
        return transformed_position
        
    #transforming back to design vector        
    def inverse_transform (self):
        
        position = np.zeros(particle.length)
        
        for i in range (particle.length):
            if self.transformed_position[i] == int(1):
                position[i] = self.lbest[i]
            elif self.transformed_position[i] == int(-1):
                position[i] = self.pbest[i]
            else:
                position[i] = random.randint(particle.lower_bound,particle.upper_bound)
        self.position = position
        return position
        
    #repair function adapted from Gesche's Differential Evolution code
    def repair(self):
        bucket = np.arange(particle.lower_bound,(particle.upper_bound)+1,1)             #bucket is a set of all feasible solution components
        self.position = np.unique(self.position)                                        #remove duplications
        for k in self.position:
            if k < particle.lower_bound or k > particle.upper_bound:
                self.position = np.delete(self.position,np.argwhere(self.position==k))  #remove out-of-bound entries
        for i in self.position:
            bucket = np.delete(bucket,np.argwhere(bucket==i))                                            #remove elements from bucket that are already in position vector to avoid repitition
        for j in range (particle.length - np.size(self.position)):                      #loop till position vector is same length as standard
            r = np.random.choice(bucket)
            self.position = np.append(self.position,r)                                  #pick a random from bucket and append to position
            bucket = np.delete(bucket,np.argwhere(bucket==r))                           #remove the already picked element from bucket to avoid repitition
            
        self.position = np.sort(self.position)            
        return self.position
        
    def update_velocity(self):
        r_p = random.random()
        r_g = random.random()
        
        for i in range (particle.length):
            self.velocity[i] = self.inertia*self.velocity[i]+r_p*self.c_p*(-1-self.transformed_position[i])+r_g*self.c_g*(1-self.transformed_position[i])
        return self.velocity
        
    def update_transformed_position(self):
        for i in range (particle.length):
            temp = self.transformed_position[i]+self.velocity[i]        
            if temp > self.alpha:
                self.transformed_position[i] = int(1)
            elif temp < -1*(self.alpha):
                self.transformed_position[i] = int(-1)
            else:
                self.transformed_position[i] = int(0)
        return self.transformed_position 
       
    def update_pbest(self):
        if isbetter(self.fitness,self.pbest_fitness,particle.maximize):
            self.pbest_fitness = np.copy(self.fitness)
            self.pbest = np.copy(self.position)
        return self.pbest, self.pbest_fitness
        
    def update_gbest(position,fitness):
        if (math.isnan(particle.gbest_fitness)):
            particle.gbest_fitness = fitness
            particle.gbest = np.copy(position)
        elif isbetter(fitness,particle.gbest_fitness,particle.maximize):
            particle.gbest_fitness = fitness
            particle.gbest = np.copy(position)
        else:
            pass
        return particle.gbest, particle.gbest_fitness
    #'''  
    #Simulation
    #def evaluate_fitness(self):
    def evaluate_fitness(self,log):
        for it_1 in range (len(log)):
            for it_2 in range (len(log[0].swarm[:,0])):
                if(np.sum(np.abs(np.sort(log[it_1].swarm[it_2,:])-np.sort(self.position)))==0):
                    self.fitness = log[it_1].fitness[it_2]
                    print ('Position already in memory, skipping simulation!')
                    return log[it_1].fitness[it_2]
        
        patch = []
        for i in range(particle.upper_bound-particle.lower_bound+1):
            patch.append('Material-Soft')
        for d in self.position:
            patch[int(d-1)] = 'Material-Stiff'
        
        inputfile = "input_temp"
        file_1 = open(inputfile+".inp", "w")
        with open("Problem_Input.inp") as file_2:
            lines = file_2.readlines()
            for i in range(27870):
                file_1.writelines(lines[i])
            
            for j in range (25):
                file_1.write('\n*Solid Section, elset=Patch-'+str(j)+', material='+str(patch[j])+'\n')
                file_1.write(',\n')
                
            for i in range(27945,28633):
                file_1.writelines(lines[i])
        file_1.close()

        #run simulation on input file
        n_cpus = 4
        s_command = 'abaqus j=' + inputfile+' cpus=' + str(n_cpus) + ' ask_delete=OFF -interactive'
        #print(s_command)
        os.system(s_command)
        
        #read ERP from output of simulation
        os.system('abaqus cae -noGUI getERP.py')
        
        #return ERP
        file_3 = open("ERP_temp.txt","r")
        ERP = ((file_3.readline()).split())[-1]
        ERP = float(ERP[:(len(ERP)-2)])
        file_3.close()
        self.fitness = ERP
        return ERP
        
    def evaluate_fitness_initial(self):
        patch = []
        for i in range(particle.upper_bound-particle.lower_bound+1):
            patch.append('Material-Soft')
        for d in self.position:
            patch[int(d-1)] = 'Material-Stiff'
        
        inputfile = "input_temp"
        file_1 = open(inputfile+".inp", "w")
        with open("Problem_Input.inp") as file_2:
            lines = file_2.readlines()
            for i in range(27870):
                file_1.writelines(lines[i])
            
            for j in range (25):
                file_1.write('\n*Solid Section, elset=Patch-'+str(j)+', material='+str(patch[j])+'\n')
                file_1.write(',\n')
                
            for i in range(27945,28633):
                file_1.writelines(lines[i])
        file_1.close()

        #run simulation on input file
        n_cpus = 4
        s_command = 'abaqus j=' + inputfile+' cpus=' + str(n_cpus) + ' ask_delete=OFF -interactive'
        #print(s_command)
        os.system(s_command)
        
        #read ERP from output of simulation
        os.system('abaqus cae -noGUI getERP.py')
        
        #return ERP
        file_3 = open("ERP_temp.txt","r")
        ERP = ((file_3.readline()).split())[-1]
        ERP = float(ERP[:(len(ERP)-2)])
        file_3.close()
        self.fitness = ERP
        return ERP
    #'''    
        
    '''
    #simple addition (unimodal)
    def evaluate_fitness(self):
        fitness = 0
        for i in range (np.size(self.position)):
            fitness += self.position[i]
        self.fitness = fitness
        return fitness
    '''
    '''
    #Two peak trap (multimodal) as defined in
    #D. Beasley, D. R. Bull, Ralph R. Martin. (1993). A sequential Niche Technique for Multimodal Function Optimization 
    #Originally defined by    
    #Ackley, D. H. (1987). An emperical study of bit vector function optimization. In L. Davis (Ed.), Genetic Algorithms and Stimulated Annealing (Ch. 13). Pitman
    def evaluate_fitness(self):
        evens = 0    
        temp = np.copy(self.position) 
        temp = temp%2
        for i in temp:
            if i ==0:
                evens+=1
            else:
                pass
        z = particle.length/2
        
        if evens < z:
            self.fitness =  180/z*(z-evens) #odd peak
        else:
            self.fitness = 200/z*(evens-z)  #even peak
        return self.fitness
    '''
    '''    
    #Simplified (Modified) Rastrigin Function
    def evaluate_fitness(self):
        evens = 0
        p = 2
        frequency = 0.5     
        temp = np.copy(self.position)
        temp = temp%2
        for i in temp:
            if i ==0:
                evens+=1
            else:
                pass
        self.fitness = 25-(0.1*((evens-p)*(evens-p))-10*np.cos(frequency*np.pi*(evens-p)+2))
        return self.fitness
    '''
    '''
    #Sphere function
    def evaluate_fitness(self):
        self.fitness = 0
        for x in self.position:
            self.fitness += x*x
        return self.fitness
    '''
    '''	
    #Rastrigin Function
    def evaluate_fitness(self):
        Amplitude = 200
        frequency = 0.35
        d = np.size(self.position)
        self.fitness = Amplitude*d
        for x in self.position:
            self.fitness += (x+2)*(x+2)-Amplitude*np.cos(frequency*np.pi*(x+2))
        return self.fitness
    '''
    '''	
    #Ackley Function
    def evaluate_fitness(self):
        a=20
        b=0.2
        c=2*np.pi
        d=len(self.position)
        p1 = 0
        p2 = 0
        for x in self.position:
            p1 += x*x
            p2 += np.cos(c*x)
        self.fitness = -a*np.exp(-b*np.sqrt(1/d*p1))-np.exp(1/d*p2) + a+np.exp(1)
        return self.fitness
    '''
    '''
    #Schwefel Function
    def evaluate_fitness(self):
        d=len(self.position)
        p1= 0
        for x in self.position:
            p1 += (x * np.sin(np.sqrt(np.abs(x))))
        self.fitness = 418.9829 * d - p1
        return self.fitness
    '''
    '''
    #Griewank Function
    def evaluate_fitness(self):
        p1 = 0
        p2 = 0
        for i in range(len(self.position)): 
            p1 += (self.position[i]/1000*self.position[i]/1000)/4000
            p2 *= np.cos(self.position[i]/1000/np.sqrt(i+1))
        self.fitness = p1-p2+1
        return self.fitness
    '''
    '''
    #Styblinski-tang Function
    def evaluate_fitness(self):
        self.fitness = 0
        for x in self.position:
            self.fitness += (x*x*x*x - 16*x*x +5*x)
        self.fitness *= 1/2
        return self.fitness
        
    '''
    #reference for test functions: https://www.sfu.ca/~ssurjano/optimization.html
    
    
#--------------------------------------------        
class archive:
    
    @classmethod    
    def initialize_global_variables(cls, dimension,swarm_size, maximize):
        archive.dimension = dimension
        archive.swarm_size = swarm_size
        archive.gbest = np.zeros(archive.dimension)
        archive.gbest_fitness = float('NaN')
        archive.maximize = maximize
    
    @classmethod
    def update_gbest(cls, position,fitness):
        if math.isnan(archive.gbest_fitness):
            archive.gbest_fitness = fitness
            archive.gbest = np.copy(position)
        elif isbetter(fitness,archive.gbest_fitness,archive.maximize):
            archive.gbest_fitness = fitness
            archive.gbest = np.copy(position)
        else:
            pass
        return archive.gbest, archive.gbest_fitness 
       
    def __init__(self):        
        self.swarm = np.empty((archive.swarm_size,archive.dimension),dtype=int)
        self.fitness = np.empty((archive.swarm_size),dtype=float)

        self.pbest_fitness = np.empty((archive.swarm_size),dtype=float)

        self.gbest = np.empty((archive.dimension),dtype=int)
        self.gbest_fitness = np.empty((1),dtype=float)
        
    def store(self,index,position,fitness,pbest_fitness):
        self.store_position(index,position)
        self.store_fitness(index,fitness)
        self.store_pbest_fitness(index,pbest_fitness)
    
    def store_position(self,index,position):
        self.swarm[index][:] = position
        
    def store_fitness(self,index,fitness):
        self.fitness[index] = fitness
        
    def store_pbest_fitness(self,index,pbest_fitness):
        self.pbest_fitness[index] = pbest_fitness
        
    def store_gbest_fitness(self,gbest_fitness):
        self.gbest_fitness = gbest_fitness
        
    def store_gbest(self,gbest):
        self.gbest = gbest
        
                   


#----------------------------------------------
def isbetter(a,b,maximize):
    if (maximize):        
        if(a>b):
            return True
        if(a<b):
            return False

    elif (not maximize):
        if(a>b):
            return False
        if(a<b):
            return True
    
    else:
        return False