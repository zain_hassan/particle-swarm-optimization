# -*- coding: utf-8 -*-
"""
Honors' Project
    "Combinatorial Multimodal Optimization using Artificial Life Algorithms"

    Supervisor: Gesche Fensder, Professorship of Computational Mechanics, Technical University of Munich
    Author:     Zain Hassan
    Date:       3 March 2017
    
Species-based Combinatorial Particle Swarm Optimization (SpeicesCPSO):
    -CPSO adapted from 'Combinatorial PSO for partitional clustering problem, Jarboui et al (2007), Elsevier'
    -SPSO adapted from 'Adaptively Choosing Neighbourhood Bests Using Species in a Particle Swarm Optimizer for Multimodal Function Optimization, Xiaodong Li (2004), Springer'
    -repair function and plotmydesign function adapted from Differential Evolution algorithm written by Gesche Fender  as a part of PhD thesis at Technical University of Munich
    
Main File
"""
import numpy as np
from classes import particle, archive
from functions import divide_into_subswarms, assemble_main_swarm, update_lbest 
from functions import write_header_to_file, print_and_save, print_all_optima
from functions import plot_pbest_fitness, plot_gbest_fitness, plot_particle_history, plotmydesign

#************INITIALIZATION************

maximize = False
minimize = not maximize

#define the problems
rows = int(5)
columns = int(5)
upper_bound = rows*columns                   
lower_bound = 1
range_ = upper_bound-lower_bound
dimension = int(np.floor(upper_bound/2))


#set parameters of SCPSO
swarm_size = 25                                                               #swarm size = number of particles (default: equal to or greater than range) 
max_gen = 200                                                                 #maximum number of generations
subswarm_radius =0#range_											              #radius of a subswarm around a seed in Euclidean distance, setting to zero will make the algorithm work with only 1 swarm
subswarm_frequency = 10000                                                    #make subswarms after how many generations
min_no_subswarms = 1
inertia = 0.85         #0.85                                                  #inertia for velocity
c_p = 0.6              #0.6                                                   #coefficient for cognitive behaviour
c_g = 0.4              #0.4                                       	          #coefficient for social behaviour
alpha = 7/swarm_size   #0.35                                                  #factor for inverse transformation in CPSO
epsilon = 0                                                                 #+-epsilon around global optimum also considered as global optima
adaptive_c_p = False                                                          #adaptively decrease c_p in every generation to force convergence

particle.set_inertia(inertia)
particle.set_c_p(c_p ) 
particle.set_c_g(c_g) 
particle.set_alpha(alpha)

#file for writing results
fo = open("run\\result.txt", "w")
write_header_to_file(fo,swarm_size,max_gen,subswarm_radius,subswarm_frequency,inertia,c_p,c_g,alpha,epsilon,adaptive_c_p,rows,columns,min_no_subswarms)
  
#initialize first main swarm and global variables
particle.initialize_global_variables(swarm_size,dimension,upper_bound,lower_bound,maximize)
main_swarm = np.empty((swarm_size), dtype=object)
for it_0 in range (swarm_size):
    main_swarm[it_0]= particle(it_0)

#set up achives
archive.initialize_global_variables(dimension,swarm_size,maximize)
log = np.empty((max_gen),dtype=object)
for it_1 in range (max_gen):
    log[it_1] = archive()


#************SOLUTION************
for g in range(max_gen):                                                       #Loop over generations
    print_and_save(fo,'\n######################## GEN: '+str(g+1))
    
    #adaptive shift from exploration to exploitation, added to improve convergence    
    if (adaptive_c_p ):
        #particle.set_c_p(c_p*(1-1/max_gen*g))                                 #linear 
        particle.set_c_p(c_p*np.exp(-g))                                       #exponential
    else:
        pass
    
    #Splitting of main swarm into subswarms
    if g%subswarm_frequency ==0:
       subswarm = divide_into_subswarms(swarm_size,subswarm_radius, min_no_subswarms, main_swarm, maximize)#based on Species based PSO
    
    update_lbest(subswarm, maximize)                                            #Update local best of the subswarm    
    
    counter=0            
    #Loop over subswarms
    for i in range(len(subswarm)):
        #Loop over particles in subswarm
        for j in range(np.size(subswarm[i])):
            
            #archiving
            log[g].store(subswarm[i][j].ID,subswarm[i][j].position,subswarm[i][j].fitness,subswarm[i][j].pbest_fitness)
            counter += 1
            
            print_and_save(fo,'\ngen# '+str(g)+' subswarm# '+str(subswarm[i][j].subswarm_ID)+' particle# '+str(subswarm[i][j].ID)+' position: '+str(subswarm[i][j].position)+' fitness: '+str(subswarm[i][j].fitness));
    
            archive.update_gbest(subswarm[i][j].position,subswarm[i][j].fitness)    #For archiving; update the global best from all particles            
    
            #SOLVE
            #subswarm[i][j].move_and_update()                                        #solve function based on CPSO
            subswarm[i][j].move_and_update(log)
        print_and_save(fo,'\nsubswarm# '+str(subswarm[i][0].subswarm_ID)+' lbest: '+str(subswarm[i][0].lbest)+' lbest fitness: '+str(subswarm[i][0].lbest_fitness)+'\n---------------------------------------');

    if ((g+1)%subswarm_frequency ==0 and (g+1)!=max_gen):
        main_swarm = assemble_main_swarm(swarm_size,subswarm)                  #assemble subswarms into main swarm, needed for re-splitting

#************POST-PROCESSING************
    log[g].store_gbest_fitness(archive.gbest_fitness)                          #save the global best at this generation
    print_and_save(fo,'\ngbest: '+str(archive.gbest)+' gbest_fitness '+str(archive.gbest_fitness));
fo.write('\n###############################################')

for it_3 in range(len(subswarm)):    
    print_and_save(fo,'\nsubswarm# '+str(subswarm[it_3][0].subswarm_ID)+' lbest: '+str(subswarm[it_3][0].lbest)+' lbest_fitness: '+str(subswarm[it_3][0].lbest_fitness))
    plotmydesign(subswarm[it_3][0].lbest,rows,columns,'lbest of Subswarm# '+str(subswarm[it_3][0].subswarm_ID)+' fitness = '+str(subswarm[it_3][0].lbest_fitness))
    
print_and_save(fo,'\n###############################################')

plotmydesign(archive.gbest,rows,columns,'gbest from the archive, fitness = '+str(archive.gbest_fitness))
print_all_optima(log,fo,epsilon)

#plotting the archive
plot_pbest_fitness(log)
plot_gbest_fitness(log)
plot_particle_history(log,1)

fo.close()