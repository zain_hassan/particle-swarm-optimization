# -*- coding: utf-8 -*-
"""
Honors' Project
    "Combinatorial Multimodal Optimization using Artificial Life Algorithms"

    Supervisor: Gesche Fensder, Professorship of Computational Mechanics, Technical University of Munich
    Author:     Zain Hassan
    Date:       3 March 2017
    
Species-based Fitness Sharing Combinatorial Particle Swarm Optimization for multimodal optimization(FS-SpeicesCPSO):
    -Combinatorial PSO adapted from 'Combinatorial PSO for partitional clustering problem, Jarboui et al (2007), Elsevier'
    -Species PSO adapted from 'Adaptively Choosing Neighbourhood Bests Using Species in a Particle Swarm Optimizer for Multimodal Function Optimization, Xiaodong Li (2004), Springer'
    -Fitness sharing adapted from 'PSO with sharing for Multimodal function optimization, Li et al (2003)'
    -Inertia reduction idea taken from 'A Dynamic Inertia weight PSO algorithm, Jiao et al (2006), Elsevier'
    -repair, getERP and plotmydesign function adapted from Differential Evolution algorithm written by Gesche Fender  as a part of PhD thesis at Technical University of Munich    
    
Main File
"""
import numpy as np
from classes_multimodal_TransferFxn import particle
from archive import archive
from functions import write_header_to_file_multimodal, divide_into_subswarms_shared, assemble_main_swarm, print_gen, plot_log, share_fitness_subswarm

#************INITIALIZATION************
np.set_printoptions(threshold=np.inf)

#define the problems
maximize = False
rows = int(4)
columns = int(8)
upper_bound = rows*columns                   
lower_bound = 1
range_ = upper_bound-lower_bound
dimension = int(np.floor(upper_bound/2))

#set parameters of SCPSO
#swarm_size = 5                                                                #swarm size = number of particles (default: equal to or greater than range) 
#max_gen = 5 
swarm_size = 75                                                                #swarm size = number of particles (default: equal to or greater than range) 
max_gen = 250                                                                  #maximum number of generations

subswarm_radius =range_/5									     #radius of a subswarm around a seed in Euclidean distance, setting to zero will make the algorithm work with only 1 swarm
subswarm_frequency = 99999                                                     #make subswarms after how many generations
min_no_subswarms = 5
sharing_radius = 1.5 #should be >1        

inertia = 0.85         #0.85                                                   #inertia for velocity
c_p = 0.6              #0.6                                                    #coefficient for cognitive behaviour
c_g = 0.4              #0.4                                       	           #coefficient for social behaviour
alpha = 7./swarm_size   #0.35                                                   #factor for inverse transformation in CPSO


archive.initialize_global_variables(dimension,swarm_size,maximize)
particle.initialize_global_variables(dimension,upper_bound,lower_bound,maximize,
                                     inertia, c_p, c_g, alpha)

#file for writing results
fo = write_header_to_file_multimodal(swarm_size,max_gen,subswarm_radius,
                                subswarm_frequency,inertia,c_p,c_g,alpha,
                                rows,columns,min_no_subswarms)
#set up achive
log = np.empty((max_gen),dtype=object)
for it_1 in range (max_gen):
    log[it_1] = archive()
  
#initialize first main swarm
main_swarm = np.empty((swarm_size), dtype=object)
for it_0 in range (swarm_size):
    main_swarm[it_0]= particle(it_0)

#divide the main swarm into subswarms
subswarm = divide_into_subswarms_shared(swarm_size,subswarm_radius, 
                                        min_no_subswarms, main_swarm,
                                        maximize,columns)                   

#evaluate initial fitnesses of all particles
for i in range(len(subswarm)):
    for j in range(np.size(subswarm[i])):
        subswarm[i][j].evaluate_fitness_initial()
    
#set the shared fitnesses, pbests and lbests of all particles
share_fitness_subswarm(subswarm,maximize,sharing_radius,columns)

#************SOLUTION************
for g in range(max_gen):                                                       #Loop over generations
    
    print_gen(fo, g)
    particle.update_inertia(g, max_gen, max_gen/2)

    for i in range(len(subswarm)):                                             #Loop over subswarms
        subswarm[i][0].print_lbest(fo)        
        for j in range(np.size(subswarm[i])):                                  #Loop over particles in subswarm
            
            log[g].store(subswarm[i][j])                                       #archiving
            subswarm[i][j].print_particle(fo,g+1)            
            archive.update_gbest(subswarm[i][j])                               #For archiving; update the global best from all particles            
    
            #SOLVE
            if ((g+1)!=max_gen):
                subswarm[i][j].move_and_update(log)                            #solve function based on CPSO
            
    #Splitting of main swarm into subswarms
    if ((g+1)%subswarm_frequency ==0 and (g+1)!=(max_gen)):
        main_swarm = assemble_main_swarm(swarm_size,subswarm)                  #needed for re-splitting
        subswarm = divide_into_subswarms_shared(swarm_size,subswarm_radius, 
                                               min_no_subswarms, main_swarm,
                                               maximize,columns)                       #based on Species based PSO
    
    #set the shared fitnesses, pbests and lbests of all particles
    share_fitness_subswarm(subswarm,maximize,sharing_radius,columns)
    
#************POST-PROCESSING************
    log[g].store_gbest_fitness(archive.gbest_fitness,particle.number_of_simulations)                          #save the global best at this generation
    archive.print_gbest(fo)

particle.print_number_of_function_calls(fo)

for it_3 in range(len(subswarm)):                                              #plot all lbests
    subswarm[it_3][0].print_plot_lbest(fo,rows,columns)    

archive.print_plot_optimum(fo,rows,columns)
plot_log(log)

fo.close()